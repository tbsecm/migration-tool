﻿using System.Data.OracleClient;
using System.Data.SqlClient;

namespace Turner.MSS.Test.Data
{
    public sealed class DBConnection
    {
        //JM: This is for implementing the connection as a Singleton class for implementing a single connection.

        static DBConnection()
        { }

        private DBConnection()
        { }

        public static OracleConnection GetOracleConnectionObject(string strConnectionString)
        {
            OracleConnection oracleDBConnection = new OracleConnection(strConnectionString);
            oracleDBConnection.Open();

            return oracleDBConnection;
        }

        public static SqlConnection GetSqlServerConnectionObject(string strConnectionString)
        {
            SqlConnection sqlServerConnection = new SqlConnection(strConnectionString);

            return sqlServerConnection;
        }
    }
}
