﻿using System;
using System.Data;
using System.Data.OracleClient;
using System.Data.SqlClient;

namespace Turner.MSS.Test.Data
{
    public class DAL
    {
        public static DataSet GetOracleDataSet(string strConnectionString, string strQueryStatement)
        {
            DataSet dsForDataReturn = new DataSet();

            OracleConnection oracleConnection = DBConnection.GetOracleConnectionObject(strConnectionString);

            try
            {
                using (OracleDataAdapter odAdapter = new OracleDataAdapter(strQueryStatement, oracleConnection))
                {
                    odAdapter.Fill(dsForDataReturn);
                }
            }
            catch (OracleException oEx)
            {
                throw new Exception("DAL OracleException Message : " + oEx.Message + ", Query : " + strQueryStatement, oEx);
            }
            catch (Exception ex)
            {
                throw new Exception("DAL Exception Message : " + ex.Message + ", Query : " + strQueryStatement, ex);
            }
            finally
            {
                oracleConnection.Close();
            }

            return dsForDataReturn;
        }

        public static DataSet GetOraclePagedDataSet(string strConnectionString, string strQueryStatement, int startRow)
        {
            DataSet dsForDataReturn = new DataSet();

            OracleConnection oracleConnection = DBConnection.GetOracleConnectionObject(strConnectionString);

            try
            {
                using (OracleDataAdapter odAdapter = new OracleDataAdapter(strQueryStatement, oracleConnection))
                {
                    odAdapter.Fill(dsForDataReturn,startRow,1000, strQueryStatement);
                }
            }
            catch (OracleException oEx)
            {
                throw new Exception("DAL OracleException Message : " + oEx.Message + ", Query : " + strQueryStatement, oEx);
            }
            catch (Exception ex)
            {
                throw new Exception("DAL Exception Message : " + ex.Message + ", Query : " + strQueryStatement, ex);
            }
            finally
            {
                oracleConnection.Close();
            }

            return dsForDataReturn;
        }

        public static DataSet GetSQLServerDataSet(string strConnectionString, string strQueryStatement)
        {
            DataSet dsForDataReturn = new DataSet();

            SqlConnection sqlServerConnection = DBConnection.GetSqlServerConnectionObject(strConnectionString);

            try
            {
                using (SqlDataAdapter sqldAdapter = new SqlDataAdapter(strQueryStatement, sqlServerConnection))
                {
                    sqldAdapter.Fill(dsForDataReturn);
                }
            }
            catch (SqlException SqlEx)
            {
                throw new Exception("DAL SQL Server Exception Message : " + SqlEx.Message + ", Query : " + strQueryStatement, SqlEx);
            }
            catch (Exception ex)
            {
                throw new Exception("DAL Exception Message : " + ex.Message + ", Query : " + strQueryStatement, ex);
            }
            finally
            {
                sqlServerConnection.Close();
            }

            return dsForDataReturn;
        }

        public static void PerformOracleQuery(string strConnectionString, string strQueryStatement)
        {
            OracleConnection oracleConnection = DBConnection.GetOracleConnectionObject(strConnectionString);

            try
            {
                using (OracleDataAdapter odAdapter = new OracleDataAdapter(strQueryStatement, oracleConnection))
                {
                    OracleCommand oracleCommand = new OracleCommand(strQueryStatement);

                    odAdapter.DeleteCommand = oracleCommand;
                }
            }
            catch (OracleException oEx)
            {
                throw new Exception("DAL Oracle Exception Message : " + oEx.Message + ", Query : " + strQueryStatement, oEx);
            }
            catch (Exception ex)
            {
                throw new Exception("DAL Exception Message : " + ex.Message + ", Query : " + strQueryStatement, ex);
            }
            finally
            {
                oracleConnection.Close();
            }
        }

        public static void ExecuteSQLStatement(string strConnectionString, string strSQLStatement)
        {
            try
            {
                using (SqlConnection sqlServerConnection = new SqlConnection(strConnectionString))
                {
                    SqlCommand sqlServerCommand = new SqlCommand(strSQLStatement, sqlServerConnection);

                    sqlServerConnection.Open();

                    sqlServerCommand.ExecuteNonQuery();

                    sqlServerConnection.Close();
                }
            }
            catch (SqlException oEx)
            {
                throw new Exception("DAL SQL Server Exception Message : " + oEx.Message + ", Query : " + strSQLStatement, oEx);
            }
            catch (Exception ex)
            {
                throw new Exception("DAL Exception Message : " + ex.Message + ", Query : " + strSQLStatement, ex);
            }
        }

        public static void PerformSqlServerQuery(string strConnectionString, string strQueryStatement)
        {
            try
            {
                using (SqlConnection sqlServerConnection = new SqlConnection(strConnectionString))
                {
                    SqlCommand sqlServerCommand = new SqlCommand(strQueryStatement, sqlServerConnection);

                    sqlServerConnection.Open();

                    sqlServerCommand.ExecuteNonQuery();

                    sqlServerConnection.Close();
                }
            }
            catch (SqlException oEx)
            {
                throw new Exception("DAL SQL Server Exception Message : " + oEx.Message + ", Query : " + strQueryStatement, oEx);
            }
            catch (Exception ex)
            {
                throw new Exception("DAL Exception Message : " + ex.Message + ", Query : " + strQueryStatement, ex);
            }
        }

        public static void PerformOracleInsert(string strConnectionString, DataSet dsOracleRecords)
        {
            try
            {
                using (OracleConnection oracleConnection = new OracleConnection(strConnectionString))
                {
                    oracleConnection.Open();

                    OracleTransaction oracleTransaction = oracleConnection.BeginTransaction();

                    string sqlText = "INSERT INTO [TPASControlData] VALUES(@TPASBatchId, @TPASDocumentId, @TPASCreateDate)";

                    foreach (DataRow oracleRow in dsOracleRecords.Tables[0].Rows)
                    {
                        OracleCommand oracleCommand = new OracleCommand(sqlText, oracleConnection, oracleTransaction);

                        oracleCommand.Parameters.AddWithValue("@TPASBatchId", oracleRow["batch_id"]);
                        oracleCommand.Parameters.AddWithValue("@TPASDocumentId", oracleRow["document_id"]);
                        oracleCommand.Parameters.AddWithValue("@TPASCreateDate", oracleRow["create_dt"]);

                        oracleCommand.ExecuteNonQuery();
                    }

                    oracleTransaction.Commit();
                    oracleConnection.Close();
                }
            }
            catch (OracleException oEx)
            {
                throw new Exception("DAL Oracle Exception Message : " + oEx.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("DAL Exception Message : " + ex.Message, ex);
            }
        }

        public static void PerformSQLServerInsert(string strConnectionString, DataSet dsSQLServerRecords)
        {
            try
            {
                using (SqlConnection sqlServerConnection = new SqlConnection(strConnectionString))
                {
                    sqlServerConnection.Open();

                    SqlTransaction sqlServerTransaction = sqlServerConnection.BeginTransaction();

                    string sqlText = "INSERT INTO [KofaxControlData] VALUES(@CreationDTM, @BatchID, @DocNumber, @BatchSrc)";

                    foreach (DataRow aqlServerRow in dsSQLServerRecords.Tables[0].Rows)
                    {
                        SqlCommand sqlServerCommand = new SqlCommand(sqlText, sqlServerConnection, sqlServerTransaction);

                        sqlServerCommand.Parameters.AddWithValue("@CreationDTM", aqlServerRow["CreationDTM"]);
                        sqlServerCommand.Parameters.AddWithValue("@BatchID", aqlServerRow["BatchID"]);
                        sqlServerCommand.Parameters.AddWithValue("@DocNumber", aqlServerRow["DocNumber"]);
                        sqlServerCommand.Parameters.AddWithValue("@BatchSrc", aqlServerRow["BatchSrc"]);

                        sqlServerCommand.ExecuteNonQuery();
                    }

                    sqlServerTransaction.Commit();
                    sqlServerConnection.Close();
                }
            }
            catch (SqlException oEx)
            {
                throw new Exception("DAL SQL Server Exception Message : " + oEx.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("DAL Exception Message : " + ex.Message, ex);
            }
        }

        public static string GetOracleSingleValue(string strConnectionString, string strQueryStatement)
        {
            string strRetVal = "NA";

            OracleConnection oracleConnection = DBConnection.GetOracleConnectionObject(strConnectionString);

            try
            {
                if (oracleConnection.State == ConnectionState.Closed)
                {
                    oracleConnection.Open();
                }

                using (OracleCommand oracleCommand = new OracleCommand(strQueryStatement, oracleConnection))
                {
                    Object oRetVal = oracleCommand.ExecuteScalar();

                    if (oRetVal != null)
                    {
                        strRetVal = oRetVal.ToString();
                    }
                }
            }
            catch (OracleException oEx)
            {
                throw new Exception("DAL Oracle Exception Message : " + oEx.Message + ", Query : " + strQueryStatement, oEx);
            }
            catch (Exception ex)
            {
                throw new Exception("DAL Exception Message : " + ex.Message + ", Query : " + strQueryStatement, ex);
            }
            finally
            {
                oracleConnection.Close();
            }

            return strRetVal;
        }

        public static DataSet GetRemedyTicketDataSet(string strConnectionString, string strQueryStatement)
        {
            DataSet dsForDataReturn = new DataSet();

            SqlConnection sqlServerConnection = DBConnection.GetSqlServerConnectionObject(strConnectionString);
            try
            {
                using (SqlDataAdapter sqldAdapter = new SqlDataAdapter(strQueryStatement, sqlServerConnection))
                {
                    sqldAdapter.Fill(dsForDataReturn);
                }
            }
            catch (SqlException SqlEx)
            {
                throw new Exception("DAL SQL Server Exception Message : " + SqlEx.Message + ", Query : " + strQueryStatement, SqlEx);
            }
            catch (Exception ex)
            {
                throw new Exception("DAL Exception Message : " + ex.Message + ", Query : " + strQueryStatement, ex);
            }
            finally
            {
                sqlServerConnection.Close();
            }

            return dsForDataReturn;
        }

        public static void PerformRemedyRecordInsert(string strConnectionString, string strQueryStatement)
        {
            try
            {
                using (SqlConnection sqlServerConnection = new SqlConnection(strConnectionString))
                {
                    SqlCommand sqlServerCommand = new SqlCommand(strQueryStatement, sqlServerConnection);

                    sqlServerConnection.Open();

                    sqlServerCommand.ExecuteNonQuery();

                    sqlServerConnection.Close();
                }
            }
            catch (SqlException oEx)
            {
                throw new Exception("DAL SQL Server Exception Message : " + oEx.Message + ", Query : " + strQueryStatement, oEx);
            }
            catch (Exception ex)
            {
                throw new Exception("DAL Exception Message : " + ex.Message + ", Query : " + strQueryStatement, ex);
            }
        }
    }
}
