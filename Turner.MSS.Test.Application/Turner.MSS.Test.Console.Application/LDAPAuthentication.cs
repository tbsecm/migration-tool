﻿using System;
using System.DirectoryServices;
using System.Text;
using Turner.MSS.Logging;

namespace Turner.MSS.Test.Console.Application
{
    /// <summary>
    /// Summary description for LDAPAuthentication
    /// </summary>
    public class LDAPAuthentication
    {
        private String _path;
        private String _filterAttribute;
        //private ILogger logger = new Logger();
        private static ILogger logger = new Logger();

        public LDAPAuthentication(String path)
        {
            _path = path;
        }

        public bool IsAuthenticated(String domain, String username, String pwd)
        {
            String domainAndUsername = domain + @"\" + username;
            DirectoryEntry entry = new DirectoryEntry(_path, domainAndUsername, pwd);

            try
            {	//Bind to the native AdsObject to force authentication.			
                Object obj = entry.NativeObject;

                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "(SAMAccountName=" + username + ")";
                search.PropertiesToLoad.Add("cn");

                SearchResult result = search.FindOne();

                if (null == result)
                {
                    return false;
                }

                //Update the new path to the user in the directory.
                _path = result.Path;
                _filterAttribute = (String)result.Properties["cn"][0];
            }
            catch (Exception ex)
            {
                logger.LogError("CRD Error authenticating user: NT Id=" + username);
                logger.LogException(ex);

                return false;
            }

            return true;
        }

        public String GetGroups()
        {
            DirectorySearcher search = new DirectorySearcher(_path);

            search.Filter = "(cn=" + _filterAttribute + ")";
            search.PropertiesToLoad.Add("memberOf");

            StringBuilder groupNames = new StringBuilder();

            try
            {
                SearchResult result = search.FindOne();

                int propertyCount = result.Properties["memberOf"].Count;

                String dn;

                int equalsIndex, commaIndex;

                for (int propertyCounter = 0; propertyCounter < propertyCount; propertyCounter++)
                {
                    dn = (String)result.Properties["memberOf"][propertyCounter];

                    equalsIndex = dn.IndexOf("=", 1);
                    commaIndex = dn.IndexOf(",", 1);

                    if (-1 == equalsIndex)
                    {
                        return null;
                    }

                    groupNames.Append(dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1));
                    groupNames.Append("|");
                }
            }
            catch (Exception ex)
            {
                //throw new Exception("Error obtaining group names. " + ex.Message);
                logger.LogError("CRD Error obtaining group names:");
                logger.LogException(ex);
            }

            return groupNames.ToString();
        }

        public string GetADUserGroups(string domain, string userName, string pwd)
        {
            String domainAndUsername = domain + @"\" + userName;
            DirectoryEntry entry = new DirectoryEntry(_path, domainAndUsername, pwd);

            DirectorySearcher search = new DirectorySearcher(entry);
            //DirectorySearcher search = new DirectorySearcher(_path);
            search.Filter = String.Format("(cn={0})", userName);
            search.PropertiesToLoad.Add("memberOf");
            StringBuilder groupsList = new StringBuilder();

            SearchResult result = search.FindOne();

            try
            {
                if (result != null)
                {
                    int groupCount = result.Properties["memberOf"].Count;

                    for (int counter = 0; counter < groupCount; counter++)
                    {
                        groupsList.Append((string)result.Properties["memberOf"][counter]);
                        groupsList.Append("|");
                    }
                }

                if (groupsList.Length > 0)
                {
                    groupsList.Length -= 1; //remove the last '|' symbol
                }
            }
            catch (Exception ex)
            {
                //throw new Exception("Error obtaining group names. " + ex.Message);
                logger.LogError("CRD Error obtaining group names:");
                logger.LogException(ex);
            }

            return groupsList.ToString();
        }
    }
}