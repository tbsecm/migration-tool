﻿using System;
using System.IO;
using System.Reflection;
using System.Xml;
using Turner.MSS.Logging;

namespace Turner.MSS.Test.Console.Application
{
    static class Config
    {
        #region members

        //Log4Net
        //private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static ILogger logger = new Logger();

        #endregion members

        #region constructors

        static Config()
		{ /* None necessary */ }

        #endregion //constructors

        #region properties

        private static string _source = null;
        public static string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        private static string _table = null;
        public static string Table
        {
            get { return _table; }
            set { _table = value; }
        }

        private static string _connection = null;
        public static string Connection
        {
            get { return _connection; }
            set { _connection = value; }
        }

        private static int _groupId = -1;
        public static int GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }

        private static string _imageSearchName = null;
        public static string ImageSearchName
        {
            get { return _imageSearchName; }
            set { _imageSearchName = value; }
        }

        private static string _connectionString = null;
        public static string ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }

        #endregion //properties

        #region methods

        /// <summary>
        /// Load Config Data from source.
        /// </summary>
        /// <param name="source">Path to source XML Config file.</param>
        /// <returns></returns>
        public static bool LoadConfigurationBySource(string source)
        {
            try
            {
                //_log.DebugFormat("Attempting to load configuration for {0}.", source);

                XmlDocument xmlDocument = new XmlDocument();

                string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                path = path + "\\IPMImageConfig.xml";

                if (!File.Exists(path))
                {
                    throw new ArgumentNullException("Cannot find config file :" + path);
                }
                
                xmlDocument.Load(path);

                //Root
                XmlNode xmlNodeRoot = xmlDocument.SelectSingleNode("./root");

                //Connection String
                var encryptionUtility = new DMSEncryption();
                
                XmlNode xmlNodeCs = xmlNodeRoot.SelectSingleNode("./connectionString");

                ConnectionString = encryptionUtility.Decrypt(xmlNodeCs.InnerText);
                ConnectionString = xmlNodeCs.InnerText;

                XmlNode xmlNodeGroups = xmlNodeRoot.SelectSingleNode("./groups");
                
                foreach (XmlNode group in xmlNodeGroups.ChildNodes)
                {
                    if (group.Attributes["source"].Value == source)
                    {
                        Source = source;
                        Table = group.Attributes["table"].Value;
                        Connection = group.Attributes["imagingConnection"].Value;
                        ImageSearchName = group.Attributes["imageSearchName"].Value;
                        GroupId = Convert.ToInt16(group.Attributes["groupId"].Value);
                    }
                }

                return Validate();
            }
            catch (Exception ex)
            {
                //_log.Fatal("Cannot load configuration data.", ex);
                return false;
            }
        }

        static bool Validate()
        {
            if (String.IsNullOrEmpty(ConnectionString))
            {
                return false;
            }

            if (String.IsNullOrEmpty(Source))
            {
                return false;
            }

            if (String.IsNullOrEmpty(Table))
            {
                return false;
            }

            if (String.IsNullOrEmpty(Connection))
            {
                return false;
            }

            if (String.IsNullOrEmpty(ImageSearchName))
            {
                return false;
            }

            if (GroupId < 0)
            {
                return false;
            }

            return true;
        }

#endregion //methods
    }
}
