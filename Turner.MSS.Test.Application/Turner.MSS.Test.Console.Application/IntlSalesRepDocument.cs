﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Turner.MSS.Test.Console.Application
{
    public class IntlSalesRepDocument
    {
        private int RecIdField;
        private string _PKGID = "";
        private string mimeType;
        private string providerId;
        private string indexName;
        private string indexProvider;
        private string pageCount;
        private string _nonPKGFileName;
        private string _nonPKGFilePath;

        private string orderdocid;
        private string orderbatchnum;
        private string orderscandate;
        private string orderscantime;
        private string orderdoctype;
        private string ordersalesoffice;
        private string orderrep;
        private string orderadvertisernbr;
        private string orderadvertiser;
        private string orderbrand;
        private string ordernbr;
        private string ordernetwork;
        private string ordertrafficnbr;
        private string orderrevisionnbr;
        private string orderdaterecieved;
        private string orderflightbegindate;
        private string orderflightenddate;
        private string orderofficenbr;
        private string orderpagecount;
        private string orderrecid;
        private string year;
        public string ExportedFilePath { get; set; }
        public string UniqueId { get; set; }

        /// <remarks/>
        public string PKGID
        {
            get
            {
                return this._PKGID;
            }
            set
            {
                this._PKGID = value;
            }

        }

        /// <remarks/>
        public string MimeType
        {
            get
            {
                return this.mimeType;
            }
            set
            {
                this.mimeType = value;
            }

        }

        /// <remarks/>
        public string NonPKGFileName
        {
            get
            {
                return this._nonPKGFileName;
            }
            set
            {
                this._nonPKGFileName = value;
            }

        }

        /// <remarks/>
        public string NonPKGFilePath
        {
            get
            {
                return this._nonPKGFilePath;
            }
            set
            {
                this._nonPKGFilePath = value;
            }

        }

        /// <remarks/>
        public string ProviderId
        {
            get
            {
                return this.providerId;
            }
            set
            {
                this.providerId  = value;
            }

        }

        /// <remarks/>
        public string IndexName
        {
            get
            {
                return this.indexName;
            }
            set
            {
                this.indexName  = value;
            }

        }

        /// <remarks/>
        public string IndexProvider
        {
            get
            {
                return this.indexProvider;
            }
            set
            {
                this.indexProvider = value;
            }

        }

        /// <remarks/>
        public string RecId
        {
            get
            {
                return this.orderrecid;
            }
            set
            {
                this.orderrecid = value;
            }
        }

        /// <remarks/>
        public string PageCount
        {
            get
            {
                return this.pageCount;
            }
            set
            {
                this.pageCount  = value;
            }

        }

        /// <remarks/>
        public string OrderDocId
        {
            get
            {
                return this.orderdocid; 
            }
            set
            {
                this.orderdocid = value;
            }
        }

        /// <remarks/>
        public string OrderBatchNum
        {
            get
            {
                return this.orderbatchnum; 
            }
            set
            {
                this.orderbatchnum = value;
            }
        }

        /// <remarks/>
        public string OrderScanDate
        {
            get
            {
                return this.orderscandate; 
            }
            set
            {
                this.orderscandate = value;
            }
        }

        /// <remarks/>
        public string OrderScanTime
        {
            get
            {
                return this.orderscantime; 
            }
            set
            {
                this.orderscantime = value;
            }
        }
        
        /// <remarks/>
        public string OrderDocType
        {
            get
            {
                return this.orderdoctype; ; 
            }
            set
            {
                this.orderdoctype = value;
            }
        }

        /// <remarks/>
        public string OrderSalesOffice
        {
            get
            {
                return this.ordersalesoffice; 
            }
            set
            {
                this.ordersalesoffice = value;
            }
        }

        /// <remarks/>
        public string OrderRep
        {
            get
            {
                return this.orderrep;
            }
            set
            {
                this.orderrep = value;
            }
        }

        /// <remarks/>
        public string OrderAdvertiserNbr
        {
            get
            {
                return this.orderadvertisernbr;
            }
            set
            {
                this.orderadvertisernbr = value;
            }
        }

        /// <remarks/>
        public string OrderAdvertiser
        {
            get
            {
                return this.orderadvertiser;
            }
            set
            {
                this.orderadvertiser = value;
            }
        }

        /// <remarks/>
        public string OrderBrand
        {
            get
            {
                return this.orderbrand;
            }
            set
            {
                this.orderbrand = value;
            }
        }

        /// <remarks/>
        public string OrderNbr
        {
            get
            {
                return this.ordernbr;
            }
            set
            {
                this.ordernbr = value;
            }
        }

        /// <remarks/>
        public string OrderNetwork
        {
            get
            {
                return this.ordernetwork;
            }
            set
            {
                this.ordernetwork = value;
            }
        }

        /// <remarks/>
        public string OrderTrafficNbr
        {
            get
            {
                return this.ordertrafficnbr;
            }
            set
            {
                this.ordertrafficnbr = value;
            }
        }

        /// <remarks/>
        public string OrderRevisionNbr
        {
            get
            {
                return this.orderrevisionnbr;
            }
            set
            {
                this.orderrevisionnbr = value;
            }
        }

        /// <remarks/>
        public string OrderFlightBeginDate
        {
            get
            {
                return this.orderflightbegindate;
            }
            set
            {
                this.orderflightbegindate = value;
            }
        }
        
        /// <remarks/>
        public string OrderDateReceived
        {
            get
            {
                return this.orderdaterecieved;
            }
            set
            {
                this.orderdaterecieved = value;
            }
        }

        /// <remarks/>
        public string OrderFlightEndDate
        {
            get
            {
                return this.orderflightenddate;
            }
            set
            {
                this.orderflightenddate = value;
            }
        }

        /// <remarks/>
        public string OrderOfficeNbr
        {
            get
            {
                return this.orderofficenbr;
            }
            set
            {
                this.orderofficenbr = value;
            }
        }

        /// <remarks/>
        public string OrderPageCount
        {
            get
            {
                return this.orderpagecount;
            }
            set
            {
                this.orderpagecount = value;
            }
        }

        /// <remarks/>
        public string OrderRecId
        {
            get
            {
                return this.orderrecid;
            }
            set
            {
                this.orderrecid = value;
            }
        }

        public string Year
        {
            get
            {
                return this.year;
            }
            set
            {
                this.year = value;
            }
        }        
    }
}
