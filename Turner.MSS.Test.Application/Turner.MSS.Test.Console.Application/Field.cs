﻿using System;
using System.Collections.Generic;

namespace Turner.MSS.Test.Console.Application
{
    public class Field
    {
        #region properties

        private string _fieldName;
        public string FieldName
        {
            get { return _fieldName; }
            set { _fieldName = value; }
        }

        private string _fieldTable;
        public string FieldTable
        {
            get { return _fieldTable; }
            set { _fieldTable = value; }
        }

        private string _selectedValue;
        public string SelectedValue
        {
            get { return _selectedValue; }
            set { _selectedValue = value; }
        }

        private string _displayName;
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        private List<string> _fieldValueList;
        public List<string> FieldValueList
        {
            get { return _fieldValueList; }
            set { _fieldValueList = value; }
        }

        private FieldType _typeOfField;
        public FieldType TypeOfField
        {
            get { return _typeOfField; }
            set { _typeOfField = value; }
        }

#endregion //properties

        #region constructor

        public Field(string fieldName, string displayName, FieldType fieldType, string fieldTable)
        {
            _fieldName = fieldName;
            _displayName = displayName;
            _fieldTable = fieldTable;
            _typeOfField = fieldType;
        }

        #endregion //constructor

        #region methods

        public static Field FindFieldInList(Fields fields, string displayName)
        {
            Field returnField = fields.Find(
                delegate(Field fieldCriteria)
                {
                    return fieldCriteria.DisplayName == displayName;
                }
            );

            return returnField;
        }

        #endregion //methods
    }

    public class Fields : List<Field>
    {
        #region methods

        /// <summary>
        /// Validate fields collection. Every Field must have a value.
        /// </summary>
        /// <returns>True/False</returns>
        public bool Validate()
        {
            bool blnRetVal = true;
            foreach (var field in this)
            {
                if (String.IsNullOrEmpty(field.SelectedValue))
                {
                     blnRetVal = false;
                     return blnRetVal;
                }
            }
            return blnRetVal;
        }

        public bool Validate(string strType)
        {
            bool blnRetVal = true;
            if (strType == "S")
            {
                foreach (var field in this)
                {
                    if (String.IsNullOrEmpty(field.SelectedValue))
                    {
                        if ((field.FieldName == "NOTES") || (field.FieldName == "ADVERTISER") || (field.FieldName == "AGENCY") || (field.FieldName == "AE"))
                        {
                            blnRetVal = true;
                        }
                        else
                        {
                            blnRetVal = false;
                            return blnRetVal;
                        }
                    }
                }
            }
            if (strType == "M")
            {
                blnRetVal = false;
                foreach (var field in this)
                {
                    if (!String.IsNullOrEmpty(field.SelectedValue))
                    {
                         blnRetVal = true;
                    }
                }
            }
            return blnRetVal;
        }

        #endregion //methods
    }

    public enum FieldType
    {
        Picklist,
        TextMulti,
        TextSingle,
        CheckedListBox,
        Date
    }
}
