﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OTACORDELib;
using OTCONTEXTLib;
//using Stellent.IBPM.Interop.Imaging;

namespace Turner.MSS.Test.Console.Application
{
    public class IPMExtract
    {
        public void ExportIPMDocument()
        {
            var objNamedQueries = new NamedQueries();
            var m_objUserToken = new UserClass();

            //m_objUserToken.PrivateLogin("optika", "", false, enuPrivateLoginStyle.otPrivateLoginStyle_None);
            m_objUserToken.Login("optika", "p4=fvYwv", false);

            objNamedQueries.UserToken = m_objUserToken;
            objNamedQueries.Refresh();

            var objNamedQuery = new NamedQuery();

            //objNamedQuery.Name = objNamedQueries.("FindDoc"); 
            objNamedQuery.Name = "FindDocument"; 

            //This NamedQuery has a single prompted value requiring the PONumber being searched for
            //objNamedQuery.Conditions(1).RightValue = 123456;

            var objQCM = new QueryConnectionManager();

            objQCM.UserToken = m_objUserToken;

            var objQC = typeof (QueryConnection);
                        objNamedQuery.Name = "FindDocument";

            //var objQC = objQCM.CreateConnection;

            objQC.ExecuteQuery(objNamedQuery, RecordsetManagement.rsmUnmanaged, 1);

            var objDocInfo = new DocInfo();
            var queryStatus = typeof (QueryStatus);

            objDocInfo.ADOFields =
                objQC.GetResults(RecordsetReturn.rsrAll, queryStatus, null, null).Recordset(1).Fields;

            //We have a DocInfo, now insert a page into it
            var objExportEnvironment = new ExportEnvironment();
            var objExportedDocument = new ExportedDocument();

            objExportEnvironment.ExportBasePath = @"C:\Temp\IPMDocumentExport";
            objExportEnvironment.ExportNameSpace = "DocumentExportNameSpace";
            objExportEnvironment.ExportType = (otExportFileType) ExportFileType.etPDF;

            objExportedDocument.UserToken = m_objUserToken;

            var exportEnvironment = objExportedDocument.ExportEnvironment;

            objExportedDocument.DocumentId = objDocInfo.DocumentID;
            objExportedDocument.IndexID = objDocInfo.IndexID;

            var objDocumentManager = new DocumentManager();

            objDocumentManager.UserToken = m_objUserToken;
            objDocumentManager.WritePageObjectToFile(objDocInfo.DocumentID, "", otPageNumber.otFirstPage);
        }


        public void RetrieveIPMDocument()
        {
            //var objNamedQueries = new NamedQueries();
            var m_objUserToken = new UserClass();

            //m_objUserToken.PrivateLogin("optika", "", false, enuPrivateLoginStyle.otPrivateLoginStyle_None);
            m_objUserToken.Login("optika", "p4=fvYwv", false);

            //objNamedQueries.UserToken = m_objUserToken;
            //objNamedQueries.Refresh();

            //var objNamedQuery = new NamedQuery();

            //objNamedQuery.Name = objNamedQueries.("FindDoc"); 
            //objNamedQuery.Name = "FindDocument";

            //This NamedQuery has a single prompted value requiring the PONumber being searched for
            //objNamedQuery.Conditions(1).RightValue = 123456;

            //var objQCM = new QueryConnectionManager();

            //objQCM.UserToken = m_objUserToken;
            //objQCM.CreateConnection();

            //We have a DocInfo, now insert a page into it
            var objExportEnvironment = new ExportEnvironment();
            var objExportedDocument = new ExportedDocument();
            var objDocumentManager = new DocumentManager();
            var objDocInfo = new DocInfo();
            var exportEnvironment = objExportedDocument.ExportEnvironment;

            objExportedDocument.UserToken = m_objUserToken;
            objDocumentManager.UserToken = m_objUserToken;

            objExportEnvironment.ExportBasePath = @"C:\Temp\IPMDocumentExport";
            objExportEnvironment.ExportNameSpace = "DocumentExportNameSpace";
            objExportEnvironment.ExportType = (otExportFileType)ExportFileType.etTIF;

            objExportedDocument.DocumentId = objDocInfo.DocumentID;
            objExportedDocument.IndexID = objDocInfo.IndexID;

            objDocumentManager.WritePageObjectToFile(objDocInfo.DocumentID, @"C:\Temp\IPMDocumentExport", otPageNumber.otFirstPage);
        }
    }
}
