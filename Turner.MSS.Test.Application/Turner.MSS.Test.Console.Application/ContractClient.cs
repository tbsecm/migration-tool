﻿using Turner.MSS.Test.Console.Application.ServiceReferenceTest;

namespace Turner.MSS.Test.Console.Application
{
    class ContractClient
    {
        public static AddContractResponse TestContractClient()
        {
            ContractHeaderServiceClient headerServiceClient = new ContractHeaderServiceClient("DEV");
            ContractDC contractDC = new ContractDC();

            contractDC.DocumentId = "Test";

            AddContractRequest addContractRequest = new AddContractRequest("DEV", contractDC);
            AddContractResponse addContractResponse = headerServiceClient.InsertContractHeader(addContractRequest);

            return addContractResponse;
        }
    }
}
