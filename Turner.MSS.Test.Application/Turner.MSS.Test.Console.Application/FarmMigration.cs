﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using CEWebClientCS;

namespace Turner.MSS.Test.Console.Application
{
    public class FarmMigration
    {

        private static string fol2012 = "{F1557779-E25C-434A-85FF-944DF9BDD65C}";
        private static string fol2013 = "{BAD00C7B-BF96-46F7-8B98-F5FD4D8F9813}";
        private static string fol2014 = "{EA4E4CB8-5FAC-41F0-826D-92EF4B8E8FAC}";
        private static string fol2015 = "{2BCA7744-3CB5-4AB9-8913-3ADCCFA3C114}";
        private static string fol2016 = "{0DF7AF76-DBF1-4433-8ABD-6C596A8C6E97}";
        private static string fol2017 = "{A710CE6E-8B07-4A8D-8C9B-D5215C5B06D7}";
        private static string fol2018 = "{E52D9DA8-C927-4E77-8F05-2E2641753D6E}";
        private static string cartoonFolder = "{89B1A4BD-3F8C-41F3-823B-B428CF7FFAA5}";

        private static string yr2012 = "2012";
        private static string yr2013 = "2013";
        private static string yr2014 = "2014";
        private static string yr2015 = "2015";
        private static string yr2016 = "2016";
        private static string yr2017 = "2017";
        private static string yr2018 = "2018";

        private static string REC_FOLDER_PATH = "/IPM Repository Documents";

        public static string CreateFileNetFolder(DataRow package)
        {

            string folderId = "";
            try
            {
                CEWebClientCS.FarmFileNetOperations fnFarmFileNetOperations = new FarmFileNetOperations();

                folderId = fnFarmFileNetOperations.createFarmPackage(package, FarmMigration.fol2016, FarmMigration.yr2016);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return folderId;
        }

        public static string CreateCartoonFileNetFolder(DataRow package)
        {

            string folderId = "";
            try
            {
                CEWebClientCS.FarmFileNetOperations fnFarmFileNetOperations = new FarmFileNetOperations();

                folderId = fnFarmFileNetOperations.createCartoonPackage(package, FarmMigration.cartoonFolder);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return folderId;
        }

        public static string CreateFileNetRepDoc(DataRow document, string filePath)
        {

            string docId = "";
            try
            {
                CEWebClientCS.FarmFileNetOperations fnFarmFileNetOperations = new FarmFileNetOperations();

                docId = fnFarmFileNetOperations.CreateFarmRepDocument(document, filePath, REC_FOLDER_PATH);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return docId;
        }

        public static string CreateFileNetPackDocument(string docTitle, string mimeType, string recId, string filePath, string parentPath, string isHistory, string year)
        {
            string docId = "";
            try
            {
                CEWebClientCS.FarmFileNetOperations fnFarmFileNetOperations = new FarmFileNetOperations();

                docId = fnFarmFileNetOperations.CreateFarmDocument(filePath, mimeType, docTitle, recId, parentPath, isHistory, year);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return docId;
        }

        public static string CreateFileNetCartoonDocument(string docTitle, string mimeType, string filePath, string parentPath, DataRow document)
        {
            string docId = "";
            try
            {
                CEWebClientCS.FarmFileNetOperations fnFarmFileNetOperations = new FarmFileNetOperations();

                docId = fnFarmFileNetOperations.CreateCartoonDocument(filePath, mimeType, docTitle, parentPath, document);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return docId;
        }
    }
}
