using System.Web.Security;

namespace Turner.MSS.Test.Console.Application
{
    /// <summary>
    /// Design and Architecture: Mohammad Ashraful Alam [ashraf@mvps.org]
    /// </summary>
    public sealed class TestLogOn
    {
        private TestLogOn() { }

        public static void PerformAuthentication(string userName, string userPassword, bool remember)
        {
            //FormsAuthentication.RedirectFromLoginPage(userName, remember);

            //if (System.Web.HttpContext.Current.Request.QueryString["ReturnUrl"] == null)
            //    RedirectToDefaultPage();
            //else
            //    System.Web.HttpContext.Current.Response.Redirect(System.Web.HttpContext.Current.Request.QueryString["ReturnUrl"].ToString());
            //
        }

        public static void PerformAdminAuthentication(string userName, bool remember)
        {
            FormsAuthentication.RedirectFromLoginPage(userName, remember);

            if (System.Web.HttpContext.Current.Request.QueryString["ReturnUrl"] == null)
                RedirectToAdminDefaultPage();
            else
                System.Web.HttpContext.Current.Response.Redirect(System.Web.HttpContext.Current.Request.QueryString["ReturnUrl"].ToString());

        }

        /// <summary>
        /// Redirects the current user based on role
        /// </summary>
        public static void RedirectToDefaultPage()
        {
            //if(User.IsInRole(@"DomainName\Manager"))
            //  // Perform restricted operation
            //else
            //  // Return unauthorized access error.
  
            System.Web.HttpContext.Current.Response.Redirect("~/private/ContractsInbox.aspx");
        }

        public static void RedirectToAdminDefaultPage()
        {
            System.Web.HttpContext.Current.Response.Redirect("~/private/ContractsInbox.aspx");
        }

        public static void LogOff()
        {
            // Put user code to initialize the page here
            FormsAuthentication.SignOut();

            //// Invalidate roles token
            //Response.Cookies[Globals.UserRoles].Value = "";
            //Response.Cookies[Globals.UserRoles].Path = "/";
            //Response.Cookies[Globals.UserRoles].Expires = new System.DateTime(1999, 10, 12);

            //Set the current user as null
            System.Web.HttpContext.Current.User = null;

            System.Web.HttpContext.Current.Response.Redirect("~/public/log-off.aspx");
        }
    }
}