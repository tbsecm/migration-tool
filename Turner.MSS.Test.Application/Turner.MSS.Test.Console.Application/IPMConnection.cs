﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stellent.IBPM.Interop.Common;
using Stellent.IBPM.Interop.Imaging;

namespace Turner.MSS.Test.Console.Application
{
    class IPMConnection
    {
        private static User _userClass = null;
        private static UserToken _userToken = null;

        private static void InitiateIpmConnection()
        {
            if (_userToken != null)
            {
                ReleaseIpmUserToken();
            }

            _userClass = new User();
            _userToken = _userClass.Login("optika", "p4=fvYwv", false);
        }

        public static UserToken GetIpmUserToken()
        {
            if (_userClass == null || _userToken == null)
            {
                InitiateIpmConnection();
                return _userToken;
            }
            else
            {
                return _userToken;
            }
        }

        public static void ReleaseIpmUserToken()
        {
            if (_userToken != null)
            {
                _userToken.Logout();
            }
            _userToken = null;
            _userClass = null;
        }

    }
}
