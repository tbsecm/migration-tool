﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using Turner.EA.Millennium.Services.Contract.DataContract;
using Turner.MSS.Logging;
using Turner.MSS.Test.Console.Application.Client;
using Turner.MSS.Test.Data;
using Stellent.IBPM.Interop.Common;
using Stellent.IBPM.Interop.Imaging;
using ADODB;
using CEWebClientCS;
using iText.IO.Font;
using iText.IO.Font.Constants;
using iText.IO.Util;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Layout;
using iText.Kernel.Pdf;
using iText.Layout.Properties;
using iText.Layout.Element;
using Path = System.IO.Path;

namespace Turner.MSS.Test.Console.Application
{
    class Program
    {
        //private static Logging.ILogger logger = new Logging.Logger();
        private static ILogger logger = new Logger();

        private static ConnectionStringSettings dbConnection = ConfigurationManager.ConnectionStrings["IPMConnectionString"];

        #region IBPM Support
        //private Stellent.IBPM.Process.Forms.FormContext _formContext;
        //private Stellent.IBPM.Interop.Process.Package _package;
        //private Stellent.IBPM.Interop.Process.FieldValues _packageFieldValues;
        //private Stellent.IBPM.Client.ToolNotifier _toolNotifier;
        //private System.Windows.Forms.Label _formCaption;

        #endregion

        private static UserToken _userToken;

        public static UserToken UserToken
        {
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                _userToken = value;
            }
        }

        private static void Main(string[] args)
        {
            logger.LogInfoMessage("Migration Application Started at: " + DateTime.Now);

            //List<string> servicesList = FetchMilleMasterContractList();
            List<string> servicesList = null;

            args = new string[] { "CartoonMigration" };

            if (args.Length <= 0) return;

            foreach (string s in args)
            {
                switch (s)
                {
                    case "TCDContractMetadataExtraction":
                        ExtractContractMetaData(servicesList);
                        break;
                    case "TCDAffiliateMetadataExtraction":
                        ExtractAffiliateMeteData(servicesList);
                        break;
                    case "TCDBusDevMetadataExtraction":
                        ExtractBizDevMetaData(servicesList);
                        break;
                    case "TCDIPMContentExport":
                        ExtractImagesFromIPM();
                        break;
                    case "IntSalesRepositoryMigration":
                        ExtractIntSalesRepDocuments("16");
                        break;
                    case "IntSalesAttachmentsMigration":
                        ExtractIntSalesAttachmentDocuments("16");
                        break;
                    case "FarmPackageMigration":
                        ExtractFarmPackages("16");
                        break;
                    case "FarmRepositoryDocMigration":
                        ExtractFarmRepDocs();
                        break;
                    case "CartoonMigration":
                        CartoonMigration();
                        break;
                    case "JoinTiffs":
                        JoinTiffs();
                        break;
                    case "CreatePDF":
                        CreatePDF();
                        break;
                }
            }
        }

        private static void CreatePDF()
        {
            string dest = @"C:\temp\FARm\deepak.pdf";
            string header = "Col1;Col3;Col4";
            string row1 = "This is a great test ;this is;hope this works";

            var writer = new PdfWriter(dest);
            var pdf = new PdfDocument(writer);
            var document = new Document(pdf, PageSize.A4.Rotate());
            document.SetMargins(20, 20, 20, 20);
            var font = PdfFontFactory.CreateFont(StandardFonts.HELVETICA);
            var bold = PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD);
            var table = new iText.Layout.Element.Table(new float[] { 4, 1, 3 });
            table.SetWidth(UnitValue.CreatePercentValue(100));

            process(table, header, bold, true);
            process(table, row1, font, false);

            document.Add(table);
            document.Close();
        }

        private static void process(iText.Layout.Element.Table table, String line, PdfFont font, bool isHeader)
        {
            var tokenizer = new StringTokenizer(line, ";");
            while (tokenizer.HasMoreTokens())
            {
                if (isHeader)
                {
                    table.AddHeaderCell(
                        new Cell().Add(
                            new Paragraph(tokenizer.NextToken()).SetFont(font)));
                }
                else
                {
                    table.AddCell(
                        new Cell().Add(
                            new Paragraph(tokenizer.NextToken()).SetFont(font)));
                }
            }
        }

        private static void JoinTiffs()
        {
            ArrayList list = new ArrayList();
            list.Add(@"C:\temp\FARM\IPMDocumentExport\img116\img3F3F.tif");
            list.Add(@"C:\temp\FARM\IPMDocumentExport\img60F1\img63B0.tif");

            TiffManager tiffMgr = new TiffManager();
            tiffMgr.JoinTiffImages(list, @"C:\temp\FARM\IPMDocumentExport\deepaktest.tif",EncoderValue.CompressionLZW);
        }

        private static void ExtractIntSalesRepDocuments(string year)
        {
            logger.LogInfoMessage("Beginning Int Sales repository document migration at: " + DateTime.Now + ". For year: 20" + year);
            int rowCount = 0;
            
            try
            {
                IntlSalesRepDocument ipmDocumentRecord = null;
                

                string intSalesMetadataFile = @"C:\temp\IntSalesMigration\InputFile\IntSalesRepMetadata.csv";
                string fileHeader =
                    "FileName|DocId|BatchNum|ScanDate|ScanTime|DocumentType|SalesOffice|Rep|Network|AdvertiserNbr|Advertiser|Brand|OrderNbr|TrafficNbr|RevisionNbr|DateReceived|FlightBeginDate|FlightEndDate|OfficeNbr|PkgId|RecId|PageCount|year|Filepath";

                TextWriter intSalesMetadataFileWriter = new StreamWriter(intSalesMetadataFile, true);

                intSalesMetadataFileWriter.WriteLine(fileHeader);

                string exportConnectString =
                    ConfigurationManager.ConnectionStrings["IPMConnectionString"].ConnectionString;

                DataSet ipmExportData = BLL.GetIntSalesRepositoryDataSet(exportConnectString);

                DataTable ipmExportTable = ipmExportData.Tables[0];

                logger.LogInfoMessage("Total IPM records returned: " + ipmExportTable.Rows.Count.ToString());

                foreach (DataRow exportRecord in ipmExportTable.Rows)
                {
                    rowCount++;

                    logger.LogInfoMessage("Transaction start RowId: " + rowCount.ToString());

                    ipmDocumentRecord = null;
                    ipmDocumentRecord = new IntlSalesRepDocument();

                    ipmDocumentRecord.RecId = Convert.ToString(exportRecord["RECID"]);
                    ipmDocumentRecord.ProviderId = Convert.ToString(exportRecord["PROVIDERID"]);
                    ipmDocumentRecord.PageCount = Convert.ToString(exportRecord["PAGECOUNT"]);
                    ipmDocumentRecord.MimeType = Convert.ToString(exportRecord["MIMETYPE"]);
                    ipmDocumentRecord.OrderDocId = Convert.ToString(exportRecord["DOCID"]);
                    ipmDocumentRecord.OrderBatchNum = Convert.ToString(exportRecord["BATCHNUM"]);
                    ipmDocumentRecord.OrderScanDate = Convert.ToString(exportRecord["SCANDATE"]);
                    ipmDocumentRecord.OrderScanTime = Convert.ToString(exportRecord["SCANTIME"]);
                    ipmDocumentRecord.OrderDocType = Convert.ToString(exportRecord["DOCTYPE"]);
                    ipmDocumentRecord.OrderSalesOffice = Convert.ToString(exportRecord["SALESOFFICE"]);
                    ipmDocumentRecord.OrderNetwork = Convert.ToString(exportRecord["NETWORK"]);
                    ipmDocumentRecord.OrderRep = Convert.ToString(exportRecord["REP"]);
                    ipmDocumentRecord.OrderAdvertiserNbr = Convert.ToString(exportRecord["ADVERTISERNBR"]);
                    ipmDocumentRecord.OrderAdvertiser = Convert.ToString(exportRecord["ADVERTISER"]).Replace("'", "_").Replace(",", "_");
                    ipmDocumentRecord.OrderBrand = Convert.ToString(exportRecord["BRAND"]);
                    ipmDocumentRecord.OrderNbr = Convert.ToString(exportRecord["ORDERNBR"]);
                    ipmDocumentRecord.OrderTrafficNbr = Convert.ToString(exportRecord["TRAFFICNBR"]);
                    ipmDocumentRecord.OrderRevisionNbr = Convert.ToString(exportRecord["REVISIONNBR"]).Trim();
                    ipmDocumentRecord.OrderDateReceived = Convert.ToString(exportRecord["DATERECEIVED"]);
                    ipmDocumentRecord.OrderFlightBeginDate = Convert.ToString(exportRecord["FLIGHTBEGINDATE"]);
                    ipmDocumentRecord.OrderFlightEndDate = Convert.ToString(exportRecord["FLIGHTENDDATE"]);
                    ipmDocumentRecord.OrderOfficeNbr = Convert.ToString(exportRecord["CUSTOM1"]);
                    ipmDocumentRecord.OrderRecId = Convert.ToString(exportRecord["RECID"]);
                    ipmDocumentRecord.OrderPageCount = Convert.ToString(exportRecord["PAGECOUNT"]);
                    ipmDocumentRecord.NonPKGFileName = Convert.ToString(exportRecord["FILENAME"]).Replace("'", "_");
                    ipmDocumentRecord.Year = year.Trim();
                    ipmDocumentRecord.ExportedFilePath = ExtractIntSalesIpmRepositoryDocumentContent(ipmDocumentRecord);

                    if (ipmDocumentRecord.ExportedFilePath.Length == 0)
                    {
                        logger.LogError("Content File not successfully extracted for RECID: " + ipmDocumentRecord.RecId + ", RowId: " + rowCount.ToString());
                        logger.LogInfoMessage("Transaction failure for RECID: " + ipmDocumentRecord.RecId + ", RowId: " + rowCount.ToString());
                        continue;
                    }

                    string metadataRecordData = ipmDocumentRecord.NonPKGFileName + "|" +
                                            ipmDocumentRecord.OrderDocId + "|" +
                                            ipmDocumentRecord.OrderBatchNum + "|" +
                                            ipmDocumentRecord.OrderScanDate + "|" +
                                            ipmDocumentRecord.OrderScanTime + "|" +
                                            ipmDocumentRecord.OrderDocType + "|" +
                                            ipmDocumentRecord.OrderSalesOffice + "|" +
                                            ipmDocumentRecord.OrderRep + "|" +
                                            ipmDocumentRecord.OrderNetwork + "|" +
                                            ipmDocumentRecord.OrderAdvertiserNbr + "|" +
                                            ipmDocumentRecord.OrderAdvertiser + "|" +
                                            ipmDocumentRecord.OrderBrand + "|" +
                                            ipmDocumentRecord.OrderNbr + "|" +
                                            ipmDocumentRecord.OrderTrafficNbr + "|" +
                                            ipmDocumentRecord.OrderRevisionNbr + "|" +
                                            ipmDocumentRecord.OrderDateReceived + "|" +
                                            ipmDocumentRecord.OrderFlightBeginDate + "|" +
                                            ipmDocumentRecord.OrderFlightEndDate + "|" +
                                            ipmDocumentRecord.OrderOfficeNbr + "|" +
                                            ipmDocumentRecord.PKGID.ToString().Trim() + "|" +
                                            ipmDocumentRecord.OrderRecId + "|" +
                                            ipmDocumentRecord.OrderPageCount + "|" +
                                            ipmDocumentRecord.Year + "|" +
                                            ipmDocumentRecord.ExportedFilePath;

                    logger.LogInfoMessage("Successfully set metadata values for Rec Id: " +
                                          ipmDocumentRecord.RecId);
                    

                    intSalesMetadataFileWriter.WriteLine(metadataRecordData);

                    logger.LogInfoMessage("Successfully wrote metadata record for contract for Rec Id: " +
                                          ipmDocumentRecord.RecId);

                    logger.LogInfoMessage("Transaction success for RECID: " + ipmDocumentRecord.RecId + ", RowId: " + rowCount.ToString());
                }

                intSalesMetadataFileWriter.Close();

                
            }
            catch (Exception ex)
            {
                logger.LogError("Contract export failed.");
                logger.LogException(ex);
            }
            IPMConnection.ReleaseIpmUserToken();
            logger.LogInfoMessage("Ending Int Sales repository document migration at: " + DateTime.Now + ". For year: 20" + year);
        }

        private static void CartoonMigration()
        {
            logger.LogInfoMessage("Beginning Cartoom Package Migration at: " + DateTime.Now);
            int rowCount = 0;
            int attachCount = 0;

            try
            {
                string exportConnectString =
                    ConfigurationManager.ConnectionStrings["IPMConnectionString"].ConnectionString;

                DataSet ipmExportData = BLL.GetCartoonPackageDataSet(exportConnectString);
                DataTable ipmExportTable = ipmExportData.Tables[0];

                logger.LogInfoMessage("Total IPM packages returned: " +
                                      ipmExportTable.Rows.Count.ToString());

                foreach (DataRow exportRecord in ipmExportTable.Rows)
                {
                    rowCount++;
                    attachCount = 0;

                    logger.LogInfoMessage("Transaction start RowId: " + rowCount.ToString() + ", Package Id: " +
                                          Convert.ToString(exportRecord["PKGID"]));
                    try
                    {
                        DataSet ipmHistDS = BLL.GetFarmPackageHistoryCount(exportConnectString, Convert.ToString(exportRecord["PKGID"]));
                        DataTable ipmHistDSipmHistDS = ipmHistDS.Tables[0];

                        logger.LogInfoMessage("Checking audit entries count for Pkg Id: " +
                                              Convert.ToString(exportRecord["PKGID"]));

                        int auditEntries = Convert.ToInt32(ipmHistDSipmHistDS.Rows[0][0]);

                        /*if (auditEntries > 1000)
                        {
                            throw new Exception("LARGEHISTORY Large number of audit records for Package Id: " +
                                            Convert.ToString(exportRecord["PKGID"]));
                        }*/

                        string folderId = FarmMigration.CreateCartoonFileNetFolder(exportRecord);
                        //string folderId = "";
                        //string folderId = "{7FD134AF-9DC8-474F-BDBA-0ED054711ADA}";

                        logger.LogInfoMessage("FileNet folder success. Folder path :" + folderId + ". Pkg Id: " +
                                              Convert.ToString(exportRecord["PKGID"]));

                        //Package History
                        try
                        {
                            logger.LogInfoMessage("Package History processing for Pkg Id: " +
                                                  Convert.ToString(exportRecord["PKGID"]));

                            string outHistFileName = "PkgHistory-" + Convert.ToString(exportRecord["COL2"]).Replace("/", "_").Replace(".","_").Replace("\t","") +
                                                     "-" +
                                                     Convert.ToString(exportRecord["PKGID"]);
                            
                            string exportedHistFilePath = CreateCartoonPkgHistoryDoc(exportConnectString,
                                Convert.ToString(exportRecord["PKGID"]), outHistFileName);

                            logger.LogInfoMessage("Exported history file path for Pkg id: " +
                                                  Convert.ToString(exportRecord["PKGID"]) + " is " +
                                                  exportedHistFilePath);

                            string pkgdocId = FarmMigration.CreateFileNetCartoonDocument(outHistFileName,
                                "application/pdf", exportedHistFilePath, folderId, exportRecord);

                            logger.LogInfoMessage("FileNet Pkg History Create Doc successful for Pkg Id: " +
                                                  Convert.ToString(exportRecord["PKGID"]) + ". DocId: " +
                                                  pkgdocId);
                            logger.LogInfoMessage("Pkg History Id processing Complete Pkg Id: " +
                                                  Convert.ToString(exportRecord["PKGID"]));
                        }
                        catch (Exception ex)
                        {
                            logger.LogError("Error in processing package history Pkg Id: " +
                                            Convert.ToString(exportRecord["PKGID"]));
                            logger.LogException(ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.LogError("Error in Pkg Id: " + Convert.ToString(exportRecord["PKGID"]));
                        logger.LogException(ex);
                    }
                }
                logger.LogInfoMessage("Total packages processed in this batch: " + rowCount.ToString());
            }
            catch (Exception ex)
            {
                logger.LogError("ERROR");
                logger.LogException(ex);
            }
            logger.LogInfoMessage("Ending Cartoon package migration at: " + DateTime.Now);
        }

        private static void ExtractFarmPackages(string year)
        {
            logger.LogInfoMessage("Beginning FARM Package Migration at: " + DateTime.Now + ". For year: 20" + year);
            int rowCount = 0;
            int attachCount = 0;

            try
            {
                string exportConnectString =
                    ConfigurationManager.ConnectionStrings["IPMConnectionString"].ConnectionString;

                DataSet ipmExportData = BLL.GetFarmPackageDataSet(exportConnectString);
                DataTable ipmExportTable = ipmExportData.Tables[0];

                logger.LogInfoMessage("Total IPM packages returned for year 20" + year + ": " +
                                      ipmExportTable.Rows.Count.ToString());

                foreach (DataRow exportRecord in ipmExportTable.Rows)
                {
                    rowCount++;
                    attachCount = 0;

                    logger.LogInfoMessage("Transaction start RowId: " + rowCount.ToString() + ", Package Id: " +
                                          Convert.ToString(exportRecord["PKGID"]));
                    try
                    {
                        string folderId = FarmMigration.CreateFileNetFolder(exportRecord);
                        //string folderId = "";
                       //string folderId = "{7FD134AF-9DC8-474F-BDBA-0ED054711ADA}";

                        logger.LogInfoMessage("FileNet folder success. Folder path :" + folderId + ". Pkg Id: " +
                                              Convert.ToString(exportRecord["PKGID"]));

                        DataSet ipmAttachs = BLL.GetFarmPackageAttachs(exportConnectString,
                            Convert.ToString(exportRecord["PKGID"]));
                        DataTable ipmAttach = ipmAttachs.Tables[0];

                        logger.LogInfoMessage("Total attachments returned for Pkg: " +
                                              Convert.ToString(exportRecord["PKGID"]) + ", Count: " +
                                              ipmAttach.Rows.Count.ToString());
                        logger.LogInfoMessage("Starting attachment processing...");

                        foreach (DataRow attchment in ipmAttach.Rows)
                        {
                            attachCount++;

                            logger.LogInfoMessage("Attachment transaction start RowId: " + attachCount.ToString() +
                                                  ", Package Id: " + Convert.ToString(exportRecord["PKGID"]));

                            string uniqueId = Convert.ToString(attchment["UNIQUEID"]);
                            //uniqueId = @"\\ipmvfiler\DMSFS\traceroute.txt";

                            int uniId;
                            bool isNumeric = int.TryParse(uniqueId, out uniId);
                            try
                            {
                                if (isNumeric)
                                {
                                    //valid recId case
                                    logger.LogInfoMessage("Rec Id processing for Unique Id: " + uniqueId);
                                    string exportedFilePath = ExtractFarmIpmRepositoryDocumentContent(uniqueId);
                                    //string exportedFilePath = @"C:\Temp\Dummy_Document.pdf";

                                    logger.LogInfoMessage("Exported File path for UniqueId: " + uniqueId + " is " +
                                                          exportedFilePath);

                                    string docId =
                                        FarmMigration.CreateFileNetPackDocument(
                                            Convert.ToString(exportRecord["COL3"]).Replace("/", "_") + "-" + uniqueId,
                                            Convert.ToString(attchment["MIMETYPE"]), uniqueId, exportedFilePath,
                                            folderId, "No", "20" + year);

                                    /*string docId =
                                        FarmMigration.CreateFileNetPackDocument(
                                            Convert.ToString(exportRecord["COL3"]).Replace("/", "_") + "-" + uniqueId,
                                            "application/pdf", uniqueId, exportedFilePath,
                                            folderId, "No", "20" + year);*/

                                    logger.LogInfoMessage("FileNet Create Doc successful for Unique Id: " + uniqueId +
                                                          ". DocId: " + docId);
                                    logger.LogInfoMessage("Rec Id processing Complete Unique Id: " + uniqueId);
                                }
                                else
                                {
                                    logger.LogInfoMessage("Non-Rec Id processing for Unique Id: " + uniqueId);
                                    //case for pure attachments which doesn't have a doc recid
                                    string outFileName = "Attachment-" + Convert.ToString(exportRecord["COL3"]).Replace("/", "_") + "-" +
                                                         Convert.ToString(exportRecord["PKGID"]);
                                    string exportedFilePath = ExtractFarmAttachmentContent(uniqueId, outFileName,
                                        Convert.ToString(exportRecord["PKGID"]));

                                    string mimeType = GetAttachmentMimeType(Path.GetExtension(exportedFilePath.Trim()));

                                    logger.LogInfoMessage("Exported File path for UniqueId: " + uniqueId + " is " +
                                                          exportedFilePath);

                                    string docId =
                                        FarmMigration.CreateFileNetPackDocument(outFileName,
                                            mimeType, "", exportedFilePath, folderId, "No", "20" + year);

                                    logger.LogInfoMessage("FileNet Create Doc successful for Unique Id: " + uniqueId +
                                                          ". DocId: " + docId);
                                    logger.LogInfoMessage("Non-Rec Id processing Complete Unique Id: " + uniqueId);
                                }

                                //Package History
                                try
                                {
                                    logger.LogInfoMessage("Package History processing for Pkg Id: " +
                                                          Convert.ToString(exportRecord["PKGID"]));
                                    string outHistFileName = "PkgHistory-" + Convert.ToString(exportRecord["COL3"]).Replace("/", "_") +
                                                             "-" +
                                                             Convert.ToString(exportRecord["PKGID"]);
                                    string exportedHistFilePath = CreatePkgHistoryDoc(exportConnectString,
                                        Convert.ToString(exportRecord["PKGID"]), outHistFileName);
                                    
                                    logger.LogInfoMessage("Exported history file path for Pkg id: " +
                                                          Convert.ToString(exportRecord["PKGID"]) + " is " +
                                                          exportedHistFilePath);

                                    string pkgdocId = FarmMigration.CreateFileNetPackDocument(outHistFileName,
                                        "application/pdf", "", exportedHistFilePath, folderId, "Yes", "20" + year);
                                    logger.LogInfoMessage("FileNet Pkg History Create Doc successful for Pkg Id: " +
                                                          Convert.ToString(exportRecord["PKGID"]) + ". DocId: " +
                                                          pkgdocId);
                                    logger.LogInfoMessage("Pkg History Id processing Complete Pkg Id: " +
                                                          Convert.ToString(exportRecord["PKGID"]));
                                }
                                catch (Exception ex)
                                {
                                    logger.LogError("Error in processing package history Pkg Id: " +
                                                    Convert.ToString(exportRecord["PKGID"]));
                                    logger.LogException(ex);
                                }
                            }
                            catch (Exception ex)
                            {
                                logger.LogError("Error in Unique Id: " + uniqueId);
                                logger.LogException(ex);
                            }
                        }

                        
                    }
                    catch (Exception ex)
                    {
                        logger.LogError("Error in Pkg Id: " + Convert.ToString(exportRecord["PKGID"]));
                        logger.LogException(ex);
                    }

                    logger.LogInfoMessage("Total Attachments processed : " + attachCount.ToString());
                    logger.LogInfoMessage("Ending attachments processing for Pkg Id: " + Convert.ToString(exportRecord["PKGID"]));
                }

                logger.LogInfoMessage("Total packages processed in this batch: " + rowCount.ToString());
            }
            catch (Exception ex)
            {
                logger.LogError("ERROR");
                logger.LogException(ex);
            }
            logger.LogInfoMessage("Ending FARM package migration at: " + DateTime.Now + ". For year: 20" + year);
        }

        private static void ExtractFarmRepDocs()
        {
            logger.LogInfoMessage("Beginning FARM Repository Docs Migration at: " + DateTime.Now);
            int rowCount = 0;
            
            string recId = "";

            try
            {
                string exportConnectString =
                    ConfigurationManager.ConnectionStrings["IPMConnectionString"].ConnectionString;

                DataSet ipmExportData = BLL.GetFarmRepositoryDataSet(exportConnectString);
                DataTable ipmExportTable = ipmExportData.Tables[0];

                logger.LogInfoMessage("Total IPM Rep Docs returned: " +
                                      ipmExportTable.Rows.Count.ToString());

                foreach (DataRow exportRecord in ipmExportTable.Rows)
                {
                    rowCount++;

                    recId = Convert.ToString(exportRecord["RECID"]);

                    logger.LogInfoMessage("Transaction start RowId: " + rowCount.ToString() + ", DocRecId: " +
                                          recId);
                    try
                    {
                        
                        logger.LogInfoMessage("Rec Id processing for DocRecId: " + recId);
                        string exportedFilePath = ExtractFarmIpmRepositoryDocument(recId);
                        //string exportedFilePath = @"C:\temp\Dummy_Rep_Document.pdf";

                        logger.LogInfoMessage("Exported File path for DocRecId: " + recId + " is " +
                                              exportedFilePath);

                        string docId = FarmMigration.CreateFileNetRepDoc(exportRecord, exportedFilePath);
                        //string docId = "";

                        logger.LogInfoMessage("FileNet Create Doc successful for DocRecId Id: " + recId +
                                              ". DocId: " + docId);
                        logger.LogInfoMessage("Rec Id processing Complete DocRecId Id: " + recId);
                    }
                    catch (Exception ex)
                    {
                        logger.LogError("Error in DocRecId Id: " + recId);
                        logger.LogException(ex);
                    }
                }

                logger.LogInfoMessage("Total repository documents processed in this batch: " + rowCount.ToString());
            }
            catch (Exception ex)
            {
                logger.LogError("ERROR");
                logger.LogException(ex);
            }
            logger.LogInfoMessage("Ending FARM repository migration at: " + DateTime.Now);
        }
        private static string CreateCartoonPkgHistoryDoc(string connString, string pkgId, string fileName)
        {
            string histFile = "";
            try
            {

                histFile = @"C:\temp\Cartoon\History\" + fileName + ".pdf";
                string fileHeader =
                    "Event Name;Username;Audit Stamp;Result Event Name;Audit Message";

                var writer = new PdfWriter(histFile);
                var pdf = new PdfDocument(writer);
                var document = new Document(pdf, PageSize.A4.Rotate());
                document.SetMargins(20, 20, 20, 20);
                var font = PdfFontFactory.CreateFont(StandardFonts.HELVETICA);
                var bold = PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD);
                var table = new iText.Layout.Element.Table(new float[] { 2, 2, 2, 2, 4 });
                table.SetWidth(UnitValue.CreatePercentValue(100));

                process(table, fileHeader, bold, true);

                DataSet ipmPackageHist = BLL.GetFarmPackageHistory(connString, pkgId);
                DataTable ipmPkgHistTabble = ipmPackageHist.Tables[0];


                foreach (DataRow attchment in ipmPkgHistTabble.Rows)
                {
                    table.AddCell(
                       new Cell().Add(
                           new Paragraph(Convert.ToString(attchment["EVENTNAME"]).Trim()).SetFont(font)));

                    table.AddCell(
                       new Cell().Add(
                           new Paragraph(Convert.ToString(attchment["USERNAME"]).Trim()).SetFont(font)));

                    table.AddCell(
                       new Cell().Add(
                           new Paragraph(Convert.ToString(attchment["AUDITSTAMPTXT"]).Trim()).SetFont(font)));

                    table.AddCell(
                       new Cell().Add(
                           new Paragraph(Convert.ToString(attchment["RESULTEVENTNAME"]).Trim()).SetFont(font)));

                    table.AddCell(
                       new Cell().Add(
                           new Paragraph(Convert.ToString(attchment["AUDITMSG"]).Trim()).SetFont(font)));
                }

                document.Add(table);
                document.Close();
            }
            catch (Exception ex)
            {
                logger.LogError("History export failed.");
                logger.LogException(ex);
                throw ex;
            }
            return histFile;
        }
        private static string CreatePkgHistoryDoc(string connString, string pkgId, string fileName)
        {
            string histFile = "";
            try
            {

                histFile = @"C:\temp\FARM\History\" + fileName + ".pdf";
                string fileHeader =
                    "Event Name;Username;Audit Stamp;Result Event Name;Audit Message";

                var writer = new PdfWriter(histFile);
                var pdf = new PdfDocument(writer);
                var document = new Document(pdf, PageSize.A4.Rotate());
                document.SetMargins(20, 20, 20, 20);
                var font = PdfFontFactory.CreateFont(StandardFonts.HELVETICA);
                var bold = PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD);
                var table = new iText.Layout.Element.Table(new float[] { 2, 2, 2, 2, 4 });
                table.SetWidth(UnitValue.CreatePercentValue(100));

                process(table, fileHeader, bold, true);

                DataSet ipmPackageHist = BLL.GetFarmPackageHistory(connString, pkgId);
                DataTable ipmPkgHistTabble = ipmPackageHist.Tables[0];


                foreach (DataRow attchment in ipmPkgHistTabble.Rows)
                {
                    table.AddCell(
                       new Cell().Add(
                           new Paragraph(Convert.ToString(attchment["EVENTNAME"]).Trim()).SetFont(font)));

                    table.AddCell(
                       new Cell().Add(
                           new Paragraph(Convert.ToString(attchment["USERNAME"]).Trim()).SetFont(font)));

                    table.AddCell(
                       new Cell().Add(
                           new Paragraph(Convert.ToString(attchment["AUDITSTAMP"]).Trim()).SetFont(font)));

                    table.AddCell(
                       new Cell().Add(
                           new Paragraph(Convert.ToString(attchment["RESULTEVENTNAME"]).Trim()).SetFont(font)));

                    table.AddCell(
                       new Cell().Add(
                           new Paragraph(Convert.ToString(attchment["AUDITMSG"]).Trim()).SetFont(font)));
                }

                document.Add(table);
                document.Close();
            }
            catch (Exception ex)
            {
                logger.LogError("History export failed.");
                logger.LogException(ex);
                throw ex;
            }
            return histFile;
        }

        private static void PrepareFarmIPMRepDocExportEnv(UserToken userToken, DocInfo objDocInfo, out ExportedDocument objExportedDocument, out int pageCount)
        {
            objExportedDocument = new ExportedDocument();
            var objExportEnvironment = new ExportEnvironment();

            objExportEnvironment.ExportBasePath = @"C:\temp\FARM";
            objExportEnvironment.ExportNameSpace = "IPMDocumentExport";
            objExportEnvironment.ExportType = ExportFileType.etTIF;
            //objExportEnvironment.ExportType = ExportFileType.etNative;
            objExportEnvironment.ExportDPI = 300;
            objExportEnvironment.ExportScale = 200;


            objExportedDocument.UserToken = userToken;
            objExportedDocument.ExportEnvironment = objExportEnvironment;
            objExportedDocument.ExportEnvironment.MaxOutputPages = 199;
            objExportedDocument.DocInfo = objDocInfo;

            pageCount = objExportedDocument.Pages.Count;
        }

        private static string ExtractFarmIpmRepositoryDocument(string recid)
        {
            string exportedFilePath = "";
            UserToken userToken = IPMConnection.GetIpmUserToken();
            try
            {

                string searchName = "FARM_RECID";

                NamedQueries namedQueries = new NamedQueries();

                namedQueries.UserToken = userToken;
                namedQueries.Refresh();

                NamedQuery namedQuery = namedQueries[searchName];
                string exportRecId = recid.ToString();

                namedQuery.Conditions[1].RightValue = exportRecId;

                QueryConnectionManager queryConnectionManager = new QueryConnectionManager();
                queryConnectionManager.UserToken = userToken;

                QueryConnection queryConnection = queryConnectionManager.CreateConnection();
                queryConnection.ExecuteQuery(namedQuery, RecordsetManagement.rsmUnmanaged, 1);

                var objRecordSet = new Recordset();
                objRecordSet = queryConnection.GetResults(RecordsetReturn.rsrAll).Recordset as Recordset;
                var objDocInfo = new DocInfo();

                objDocInfo.ADOFields = objRecordSet.Fields;

                queryConnectionManager.DestroyConnection(queryConnection.ConnectionId);

                ExportedDocument objExportedDocument;
                int pageCount;
                try
                {
                    PrepareFarmIPMRepDocExportEnv(userToken, objDocInfo, out objExportedDocument, out pageCount);

                    string objExportedPath = objExportedDocument.RecombineDocument(1, pageCount).FullPath;
                    //string objExportedPath = objExportedDocument.Pages[1].Sections[1].FullPath;

                    logger.LogInfoMessage("Successfully exported content file for Rec Id: " + exportRecId);

                    exportedFilePath = objExportedPath;
                }
                catch (Exception ex)
                {
                    IPMDocExportEnv(userToken, objDocInfo, out objExportedDocument, out pageCount);
                    string objExportedPath = objExportedDocument.RecombineDocument(1, pageCount).FullPath;
                    logger.LogInfoMessage("Successfully exported content file for Rec Id: " + exportRecId);
                    exportedFilePath = objExportedPath;
                }


            }
            catch (Exception ex)
            {
                logger.LogError("Package attacment IPM document export failed.");
                logger.LogException(ex);
                IPMConnection.ReleaseIpmUserToken();
                throw ex;
            }

            IPMConnection.ReleaseIpmUserToken();
            return exportedFilePath;
        }
        private static string GetAttachmentMimeType(string filePath)
        {
            string mimeType = "";

            if (filePath != "")
            {
                string ext = Path.GetExtension(filePath);

                if (ext != "")
                {
                    if (ext == ".rtf" || ext == ".doc")
                    {
                        mimeType = "application/msword";
                    }
                    else if (ext == ".docx")
                    {
                        mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    }
                    else if (ext == ".msg")
                    {
                        mimeType = "application/vnd.ms-outlook";
                    }
                    else if (ext == ".pdf")
                    {
                        mimeType = "application/pdf";
                    }
                    else
                    {
                        mimeType = "application/x-emedia.wfuniversal";
                    }
                } 
                else
                {
                    return "application/x-emedia.wfuniversal";
                }
            }
            else
            {
                return "application/x-emedia.wfuniversal";
            }

            return mimeType;
        }

        private static void ExtractIntSalesAttachmentDocuments(string year)
        {
            logger.LogInfoMessage("Beginning Int Sales attachment document migration at: " + DateTime.Now + ". For year: 20" + year);
            int rowCount = 0;

            try
            {
                IntlSalesRepDocument ipmDocumentRecord = null;


                string intSalesMetadataFile = @"C:\temp\IntSalesMigration\InputFile\IntSalesAttchMetadata.csv";
                string fileHeader =
                    "FileName|DocId|BatchNum|ScanDate|ScanTime|DocumentType|SalesOffice|Rep|Network|AdvertiserNbr|Advertiser|Brand|OrderNbr|TrafficNbr|RevisionNbr|DateReceived|FlightBeginDate|FlightEndDate|OfficeNbr|PkgId|RecId|PageCount|year|Filepath";

                TextWriter intSalesMetadataFileWriter = new StreamWriter(intSalesMetadataFile, true);

                intSalesMetadataFileWriter.WriteLine(fileHeader);

                string exportConnectString =
                    ConfigurationManager.ConnectionStrings["IPMConnectionString"].ConnectionString;

                DataSet ipmExportData = BLL.GetIntSalesRepositoryDataSet(exportConnectString);

                DataTable ipmExportTable = ipmExportData.Tables[0];

                logger.LogInfoMessage("Total IPM records returned: " + ipmExportTable.Rows.Count.ToString());

                foreach (DataRow exportRecord in ipmExportTable.Rows)
                {
                    rowCount++;

                    logger.LogInfoMessage("Transaction start RowId: " + rowCount.ToString());

                    ipmDocumentRecord = null;
                    ipmDocumentRecord = new IntlSalesRepDocument();

                    ipmDocumentRecord.PKGID = Convert.ToString(exportRecord["PKGID"]);
                    ipmDocumentRecord.OrderRecId = Convert.ToString(exportRecord["PKGRECID"]);
                    ipmDocumentRecord.OrderDocId = Convert.ToString(exportRecord["COL1"]);
                    ipmDocumentRecord.OrderBatchNum = Convert.ToString(exportRecord["COL2"]);
                    ipmDocumentRecord.OrderScanDate = Convert.ToString(exportRecord["COL3"]);
                    ipmDocumentRecord.OrderScanTime = Convert.ToString(exportRecord["COL4"]);
                    ipmDocumentRecord.OrderDocType = Convert.ToString(exportRecord["COL5"]);
                    ipmDocumentRecord.OrderSalesOffice = Convert.ToString(exportRecord["COL6"]);
                    ipmDocumentRecord.OrderNetwork = Convert.ToString(exportRecord["COL8"]);
                    ipmDocumentRecord.OrderRep = Convert.ToString(exportRecord["COL7"]);
                    ipmDocumentRecord.OrderAdvertiserNbr = Convert.ToString(exportRecord["COL9"]);
                    ipmDocumentRecord.OrderAdvertiser = Convert.ToString(exportRecord["COL10"]).Replace("'", "_").Replace(",", "_");
                    ipmDocumentRecord.OrderBrand = Convert.ToString(exportRecord["COL11"]);
                    ipmDocumentRecord.OrderNbr = Convert.ToString(exportRecord["COL12"]);
                    ipmDocumentRecord.OrderTrafficNbr = Convert.ToString(exportRecord["COL13"]);
                    ipmDocumentRecord.OrderRevisionNbr = Convert.ToString(exportRecord["COL14"]).Trim();
                    ipmDocumentRecord.OrderDateReceived = Convert.ToString(exportRecord["COL15"]);
                    ipmDocumentRecord.OrderFlightBeginDate = Convert.ToString(exportRecord["COL16"]);
                    ipmDocumentRecord.OrderFlightEndDate = Convert.ToString(exportRecord["COL17"]);
                    ipmDocumentRecord.OrderOfficeNbr = Convert.ToString(exportRecord["COL25"]);
                    ipmDocumentRecord.NonPKGFileName = Convert.ToString(exportRecord["FILENAME"]).Replace("'", "_");
                    ipmDocumentRecord.Year = year.Trim();
                    ipmDocumentRecord.UniqueId = Convert.ToString(exportRecord["UNIQUEID"]);

                    ipmDocumentRecord.ExportedFilePath = ExtractAttachmentContent(ipmDocumentRecord.UniqueId, ipmDocumentRecord.NonPKGFileName, ipmDocumentRecord.Year, ipmDocumentRecord.OrderRecId, rowCount);

                    if (ipmDocumentRecord.ExportedFilePath.Length == 0)
                    {
                        logger.LogError("Content File not successfully extracted for PKGID: " + ipmDocumentRecord.PKGID + ", RowId: " + rowCount.ToString());
                        logger.LogInfoMessage("Transaction failure for PKGID: " + ipmDocumentRecord.PKGID + ", RowId: " + rowCount.ToString());
                        continue;
                    }

                    string metadataRecordData = ipmDocumentRecord.NonPKGFileName + "|" +
                                            ipmDocumentRecord.OrderDocId + "|" +
                                            ipmDocumentRecord.OrderBatchNum + "|" +
                                            ipmDocumentRecord.OrderScanDate + "|" +
                                            ipmDocumentRecord.OrderScanTime + "|" +
                                            ipmDocumentRecord.OrderDocType + "|" +
                                            ipmDocumentRecord.OrderSalesOffice + "|" +
                                            ipmDocumentRecord.OrderRep + "|" +
                                            ipmDocumentRecord.OrderNetwork + "|" +
                                            ipmDocumentRecord.OrderAdvertiserNbr + "|" +
                                            ipmDocumentRecord.OrderAdvertiser + "|" +
                                            ipmDocumentRecord.OrderBrand + "|" +
                                            ipmDocumentRecord.OrderNbr + "|" +
                                            ipmDocumentRecord.OrderTrafficNbr + "|" +
                                            ipmDocumentRecord.OrderRevisionNbr + "|" +
                                            ipmDocumentRecord.OrderDateReceived + "|" +
                                            ipmDocumentRecord.OrderFlightBeginDate + "|" +
                                            ipmDocumentRecord.OrderFlightEndDate + "|" +
                                            ipmDocumentRecord.OrderOfficeNbr + "|" +
                                            ipmDocumentRecord.PKGID.Trim() + "|" +
                                            ipmDocumentRecord.OrderRecId + "|" +
                                            ipmDocumentRecord.OrderPageCount + "|" +
                                            ipmDocumentRecord.Year + "|" +
                                            ipmDocumentRecord.ExportedFilePath;

                    logger.LogInfoMessage("Successfully set metadata values for PKG Id: " + ipmDocumentRecord.PKGID);


                    intSalesMetadataFileWriter.WriteLine(metadataRecordData);

                    logger.LogInfoMessage("Successfully wrote metadata record for PKG Id: " +
                                          ipmDocumentRecord.PKGID);

                    logger.LogInfoMessage("Transaction success for PKGID: " + ipmDocumentRecord.PKGID + ", RowId: " + rowCount.ToString());
                }

                intSalesMetadataFileWriter.Close();


            }
            catch (Exception ex)
            {
                logger.LogError("Contract export failed.");
                logger.LogException(ex);
            }
            IPMConnection.ReleaseIpmUserToken();
            logger.LogInfoMessage("Ending Int Sales repository document migration at: " + DateTime.Now + ". For year: 20" + year);
        }
        private static string ExtractFarmIpmRepositoryDocumentContent(string recid)
        {
            string exportedFilePath = "";
            UserToken userToken = IPMConnection.GetIpmUserToken();
            try
            {
                
                string searchName = "FARM_RECID";

                NamedQueries namedQueries = new NamedQueries();

                namedQueries.UserToken = userToken;
                namedQueries.Refresh();

                NamedQuery namedQuery = namedQueries[searchName];
                string exportRecId = recid.ToString();

                namedQuery.Conditions[1].RightValue = exportRecId;

                QueryConnectionManager queryConnectionManager = new QueryConnectionManager();
                queryConnectionManager.UserToken = userToken;

                QueryConnection queryConnection = queryConnectionManager.CreateConnection();
                queryConnection.ExecuteQuery(namedQuery, RecordsetManagement.rsmUnmanaged, 1);

                var objRecordSet = new Recordset();
                objRecordSet = queryConnection.GetResults(RecordsetReturn.rsrAll).Recordset as Recordset;
                var objDocInfo = new DocInfo();

                objDocInfo.ADOFields = objRecordSet.Fields;

                queryConnectionManager.DestroyConnection(queryConnection.ConnectionId);

                ExportedDocument objExportedDocument;
                int pageCount;
                try
                {
                    TiffManager tifMgr = new TiffManager();
                    int pages = objDocInfo.PageCount;
                    ArrayList pageList = new ArrayList();
                    string objExportedPath = "";
                    string pagePath = "";

                    objExportedPath = @"C:\temp\FARM\MergedDocs\";
                     
                    for (int i = 0; i < pages; i++)
                    {
                        PrepareFarmIPMDocExportEnv(userToken, objDocInfo, i + 1, out objExportedDocument, out pageCount);
                        pagePath = objExportedDocument.Pages[i + 1].Sections[1].FullPath;
                        pageList.Add(pagePath);

                        pagePath = "";
                    }

                    try
                    {
                        tifMgr.JoinTiffImages(pageList, objExportedPath + exportRecId + ".tif",
                            EncoderValue.CompressionLZW);
                    }
                    catch (Exception ex)
                    {
                        logger.LogError("Error in merging TIFF pages.");
                        logger.LogException(ex);
                        IPMConnection.ReleaseIpmUserToken();
                        throw ex;
                    }

                    /*PrepareFarmIPMDocExportEnv(userToken, objDocInfo, 2, out objExportedDocument, out pageCount);
                    objExportedPath = objExportedDocument.RecombineDocument(1, pageCount).FullPath;*/


                    logger.LogInfoMessage("Successfully exported content file for Rec Id: " + exportRecId);

                    exportedFilePath = objExportedPath + exportRecId + ".tif";
                    //exportedFilePath = objExportedPath;
                }
                catch (Exception ex)
                {
                    IPMDocExportEnv(userToken, objDocInfo, out objExportedDocument, out pageCount);
                    string objExportedPath = objExportedDocument.RecombineDocument(1, pageCount).FullPath;
                    logger.LogInfoMessage("Successfully exported content file for Rec Id: " + exportRecId);
                    exportedFilePath = objExportedPath;
                }
                
                
            }
            catch (Exception ex)
            {
                logger.LogError("Package attacment IPM document export failed.");
                logger.LogException(ex);
                IPMConnection.ReleaseIpmUserToken();
                throw ex;
            }

            IPMConnection.ReleaseIpmUserToken();
            return exportedFilePath;
        }
        private static string ExtractIntSalesIpmRepositoryDocumentContent(IntlSalesRepDocument docRecord)
        {
            string exportedFilePath = "";
            try
            {
                logger.LogInfoMessage("Enter method ExtractIntSalesIpmRepositoryDocumentContent()");


                UserToken userToken = IPMConnection.GetIpmUserToken();
                string searchName = "INTSALES_RECID";

                NamedQueries namedQueries = new NamedQueries();

                namedQueries.UserToken = userToken;
                namedQueries.Refresh();

                NamedQuery namedQuery = namedQueries[searchName];
                string exportRecId = docRecord.RecId.ToString();
                
                namedQuery.Conditions[1].RightValue = exportRecId;

                QueryConnectionManager queryConnectionManager = new QueryConnectionManager();
                queryConnectionManager.UserToken = userToken;

                QueryConnection queryConnection = queryConnectionManager.CreateConnection();
                queryConnection.ExecuteQuery(namedQuery, RecordsetManagement.rsmUnmanaged, 1);

                var objRecordSet = new Recordset();
                objRecordSet = queryConnection.GetResults(RecordsetReturn.rsrAll).Recordset as Recordset;
                var objDocInfo = new DocInfo();

                objDocInfo.ADOFields = objRecordSet.Fields;

                queryConnectionManager.DestroyConnection(queryConnection.ConnectionId);

                ExportedDocument objExportedDocument;
                int pageCount;
                try
                {
                    PrepareIPMDocExportEnv(userToken, objDocInfo, out objExportedDocument, out pageCount);

                    string objExportedPath = objExportedDocument.RecombineDocument(1, pageCount).FullPath;
                    //string objExportedPath = objExportedDocument.Pages[1].Sections[1].FullPath;

                    logger.LogInfoMessage("Successfully exported content file for Rec Id: " + exportRecId);
                    string partFileName = docRecord.NonPKGFileName.Replace(' ', '_');

                    string destinationFilePath = @"C:\temp\Int_Sales_Export\20" + docRecord.Year + "\\" + partFileName + Path.GetExtension(objExportedPath);
                    //string destinationFilePath = @"\\ipmvfiler\DMSFS\temp\IntlSalesDocMigration\Order\Input\20" + docRecord.Year + "\\" + docRecord.NonPKGFileName + ".TIF";

                    File.Copy(objExportedPath, destinationFilePath, true);
                    exportedFilePath = destinationFilePath;
                }
                catch (Exception ex)
                {
                    IPMDocExportEnv(userToken, objDocInfo, out objExportedDocument, out pageCount);

                    string objExportedPath = objExportedDocument.RecombineDocument(1, pageCount).FullPath;

                    logger.LogInfoMessage("Successfully exported content file for Rec Id: " + exportRecId);
                    string partFileName = docRecord.NonPKGFileName.Replace(' ', '_');

                    string destinationFilePath = @"C:\temp\Int_Sales_Export\20" + docRecord.Year + "\\" + partFileName + Path.GetExtension(objExportedPath);
                    File.Copy(objExportedPath, destinationFilePath, true);
                    exportedFilePath = destinationFilePath;
                }
                

                

                logger.LogInfoMessage("Successfully copied content file for Rec Id: " + exportRecId);
                logger.LogInfoMessage("Ending TCD IPM document export at: " + DateTime.Now);
            }
            catch (Exception ex)
            {
                logger.LogError("Int Sales Repository IPM file export failed.");
                logger.LogException(ex);
            }
            return exportedFilePath;
        }

        private static List<string> FetchMilleMasterContractList()
        {
            var request = new GetContractListOptionsRequestMessage();

            GetContractListOptionsResponseMessage response = null;

            Execute(client => response = client.GetContractListOptions(request));

            var returnedListOptions = response.Options;
            var servicesList = new List<string>();

            foreach (var servicesTypeItem in returnedListOptions.Services)
            {
                servicesList.Add(servicesTypeItem.Code);
            }

            return servicesList;
        }

        private static void ExtractContractMetaData(List<string> servicesList)
        {
            try
            {
                logger.LogInfoMessage("Beginning contract metadata migration at: " + DateTime.Now);

                string contractDestinationFileName = @"C:\temp\TCD IPM Export\Contract\ContractMetadata.csv";
                string fileHeader =
                    "Filename|AffiliateId|DocType|DocExpDate|ExpDate|ContractType|Territories|Components|AmendmentId|EffectiveDate|DocSubType|ContractId|DocEffecDate|Services|DocNotes|AffiliateName|DescDate|IPMRecId|IPMCreateDate|Filepath";

                TextWriter contractMetadataFile = new StreamWriter(contractDestinationFileName, true);

                contractMetadataFile.WriteLine(fileHeader);

                string exportConnectString =
                    ConfigurationManager.ConnectionStrings["TNSConnectionString"].ConnectionString;

                DataSet ipmExportData = BLL.GetTcdIpmDistinctContractRecords(exportConnectString);

                DataTable ipmExportTable = ipmExportData.Tables[0];

                logger.LogInfoMessage("Successfully completed initial setup for execution at: " +
                                      DateTime.Now);

                foreach (DataRow exportRecord in ipmExportTable.Rows)
                {
                    DataSet tcdRecIdData = BLL.GetTcdIpmRecIdRecord(exportConnectString,
                                                                    exportRecord["RECID"].ToString());

                    DataTable tcdRecIdTable = tcdRecIdData.Tables[0];

                    DataRow tcdRecIdRow = tcdRecIdTable.Rows[0];

                    string exportRecId = tcdRecIdRow["RECID"].ToString();
                    string dbContractId = tcdRecIdRow["MILLE_CONTRACT_ID"].ToString();
                    string metadataRecordData = "|";
                    string affiliateId = "";
                    string documentType = "";
                    string documentExpireDate = "";
                    string expireDate = "";
                    string contractType = "";
                    string territories = "";
                    string components = "";
                    string amendmentId = "";
                    string effectiveDate = "";
                    string documentSubtype = "";
                    string contractId = "";
                    string documentEffectiveDate = "";
                    string services = "";
                    string documentNotes = "";
                    string affiliateName = "";
                    string destructionDate = "";
                    string recId = "";
                    string ipmCreateDate = "";

                    affiliateId = tcdRecIdRow["AFFILNBR"].ToString().Trim();
                    affiliateName = tcdRecIdRow["AFFNAME"].ToString().Trim();
                    documentType = tcdRecIdRow["DOCTYPE"].ToString().ToUpper().Trim();
                    documentSubtype = tcdRecIdRow["DOCSUBTY"].ToString().ToUpper().Trim();
                    effectiveDate = tcdRecIdRow["EFFDATE"].ToString().Trim();
                    expireDate = tcdRecIdRow["EXPDATE"].ToString().Trim();

                    if (documentType.ToUpper() == "AMENDMENT")
                    {
                        documentEffectiveDate = tcdRecIdRow["EFFDATE"].ToString().Trim();
                        documentExpireDate = tcdRecIdRow["EXPDATE"].ToString().Trim();
                    }

                    documentNotes = tcdRecIdRow["DOCNOTES"].ToString().Trim();
                    services = tcdRecIdRow["SERVICE"].ToString().Trim();
                    contractId = tcdRecIdRow["MILLE_CONTRACT_ID"].ToString().Trim();
                    amendmentId = tcdRecIdRow["AMENDID"].ToString().Trim();
                    recId = tcdRecIdRow["RECID"].ToString().Trim();
                    ipmCreateDate = tcdRecIdRow["FILEDDATE"].ToString().Trim();
                    destructionDate = tcdRecIdRow["DESTRUCTION_DATE"].ToString().Trim();

                    //rowCount = rowCount + 1;

                    documentNotes = documentNotes.Replace("'", "''");
                    affiliateName = affiliateName.Replace("'", "''");

                    string tcdAuditSql = "INSERT INTO [TCD_Document_Migration_Audit] " +
                                         "([SourceSystem]" +
                                         ", [AffiliateId]" +
                                         ", [AffiliateName]" +
                                         ", [DocumentType]" +
                                         ", [DocumentSubtype]" +
                                         ", [ContractType]" +
                                         ", [EffectiveDate]" +
                                         ", [ExpireDate]" +
                                         ", [DocumentNotes]" +
                                         ", [Services]" +
                                         ", [Components]" +
                                         ", [Territories]" +
                                         ", [ContractId]" +
                                         ", [AmendmentId]" +
                                         ", [RecId]" +
                                         ", [IpmCreateDate]" +
                                         ", [DestructionDate]" +
                                         ", [RunDate]" +
                                         ", [RunUser]) VALUES ('TCD', '" + affiliateId +
                                         "', '" + affiliateName + "', '" + documentType + "', '" +
                                         documentSubtype + "', '" +
                                         contractType + "', '" + effectiveDate + "', '" + expireDate +
                                         "', '" + documentNotes + "', '" + services + "', '" +
                                         components + "', '" + territories + "', '" + contractId + "', '" +
                                         amendmentId + "', '" + recId + "', '" +
                                         ipmCreateDate + "', '" + destructionDate + "', '" + DateTime.Now +
                                         "', " + "'Audit'),";

                    if (!String.IsNullOrEmpty(dbContractId) && dbContractId.ToUpper() != "N/A")
                    {
                        try
                        {
                            var returnedData = GetMillenniumData(dbContractId, "Contract");

                            if (!String.IsNullOrEmpty(returnedData.AffiliateId.ToString()))
                            {
                                affiliateId = returnedData.AffiliateId.ToString().Trim();
                            }
                            if (!String.IsNullOrEmpty(returnedData.AffiliateName))
                            {
                                affiliateName = returnedData.AffiliateName.Trim();
                            }
                            if (!String.IsNullOrEmpty(returnedData.EffectiveDate.ToString()))
                            {
                                effectiveDate = returnedData.EffectiveDate.ToString().Trim();

                                //if (documentType.ToUpper() == "AMENDMENT")
                                //{
                                //    documentEffectiveDate = returnedData.EffectiveDate.ToString().Trim();
                                //}
                            }
                            if (!String.IsNullOrEmpty(returnedData.ExpirationDate.ToString()))
                            {
                                expireDate = returnedData.ExpirationDate.ToString().Trim();

                                //if (documentType.ToUpper() == "AMENDMENT")
                                //{
                                //    documentExpireDate = returnedData.ExpirationDate.ToString().Trim();
                                //}
                            }
                            if (!String.IsNullOrEmpty(returnedData.Networks) &&
                                documentType.ToUpper() != "AMENDMENT")
                            {
                                services = returnedData.Networks.Replace(",", "~").Trim();
                            }
                            if (!String.IsNullOrEmpty(returnedData.Components))
                            {
                                components = returnedData.Components.Replace(",", "~").Trim();
                            }
                            if (!String.IsNullOrEmpty(returnedData.Territories))
                            {
                                territories = returnedData.Territories.Replace(",", "~").Trim();
                            }
                            if (!String.IsNullOrEmpty(returnedData.ContractType))
                            {
                                contractType = returnedData.ContractType.Trim();
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.LogError(
                                "Mille Contract Summary service failed for contract lookup for contract id: " +
                                dbContractId);
                            logger.LogException(ex);
                        }
                    }

                    //rowCount = rowCount + 1;

                    documentNotes = documentNotes.Replace("'", "''");
                    affiliateName = affiliateName.Replace("'", "''");

                    string milleAuditSql = "('MILLE', '" + affiliateId + "', '" +
                                           affiliateName + "', '" + documentType + "', '" + documentSubtype +
                                           "', '" +
                                           contractType + "', '" + effectiveDate + "', '" + expireDate +
                                           "', '" + documentNotes + "', '" + services + "', '" +
                                           components + "', '" + territories + "', '" + contractId + "', '" +
                                           amendmentId + "', '" + recId + "', '" +
                                           ipmCreateDate + "', '" + destructionDate + "', '" + DateTime.Now +
                                           "', " + "'Audit'),";


                    if (!String.IsNullOrEmpty(contractId) && documentType.ToUpper() == "AMENDMENT")
                    {
                        services = MassageServices(services);
                        services = ValidateServicesFromMille(servicesList, services);
                    }

                    if (!String.IsNullOrEmpty(services) && services.Substring(0, 1) == "~")
                    {
                        services = services.Remove(0, 1);
                    }

                    contractId = contractId.Replace("N/A", "").Trim();

                    if (!String.IsNullOrEmpty(effectiveDate) && effectiveDate == "1/1/1950 12:00:00 AM")
                    {
                        effectiveDate = "";
                    }

                    if (!String.IsNullOrEmpty(expireDate) && expireDate == "1/1/1950 12:00:00 AM")
                    {
                        expireDate = "";
                    }

                    if (!String.IsNullOrEmpty(documentEffectiveDate) && documentEffectiveDate == "1/1/1950 12:00:00 AM")
                    {
                        documentExpireDate = "";
                    }

                    if (!String.IsNullOrEmpty(documentExpireDate) && documentExpireDate == "1/1/1950 12:00:00 AM")
                    {
                        documentExpireDate = "";
                    }

                    logger.LogInfoMessage("Successfully set metadata values from MILLE for Rec Id: " +
                                          exportRecId);

                    string pathValue = @"\\cnnecmfs\filenetdv\TCD_DOC_Migration\Contract\Input\" + recId + ".TIF";

                    metadataRecordData = recId + ".TIF" + "|" + affiliateId + "|" + documentType + "|" +
                                         documentExpireDate + "|" + expireDate + "|" + contractType +
                                         "|" +
                                         territories + "|" + components + "|" + amendmentId + "|" +
                                         effectiveDate + "|" + documentSubtype + "|" + contractId + "|" +
                                         documentEffectiveDate + "|" + services + "|" + documentNotes +
                                         "|" +
                                         affiliateName + "|" + destructionDate + "|" + recId + "|" +
                                         ipmCreateDate + "|" + pathValue;

                    contractMetadataFile.WriteLine(metadataRecordData);

                    logger.LogInfoMessage("Successfully wrote metadata record for contract for Rec Id: " +
                                          exportRecId);

                    //File.Copy(@"C:\temp\TCD IPM Export\imgD461.TIF", @"C:\temp\TCD IPM Export\Contract\" + recId + ".TIF", true);
                    //rowCount = rowCount + 1;

                    documentNotes = documentNotes.Replace("'", "''");
                    affiliateName = affiliateName.Replace("'", "''");

                    string finalAuditSql = "('Final', '" + affiliateId + "', '" +
                                           affiliateName + "', '" + documentType + "', '" + documentSubtype +
                                           "', '" +
                                           contractType + "', '" + effectiveDate + "', '" + expireDate +
                                           "', '" + documentNotes + "', '" + services + "', '" +
                                           components + "', '" + territories + "', '" + contractId + "', '" +
                                           amendmentId + "', '" + recId + "', '" +
                                           ipmCreateDate + "', '" + destructionDate + "', '" + DateTime.Now +
                                           "', " + "'Audit')";

                    tcdAuditSql = tcdAuditSql + milleAuditSql + finalAuditSql;

                    BLL.InsertTcdMigrationAuditRecords(exportConnectString, tcdAuditSql);

                    logger.LogInfoMessage("Successfully inserted audit records for Rec Id: " + exportRecId);
                }

                contractMetadataFile.Close();

                logger.LogInfoMessage("Ending contract document migration at: " + DateTime.Now);
            }
            catch (Exception ex)
            {
                logger.LogError("Contract export failed.");
                logger.LogException(ex);
            }
        }

        private static string ValidateServicesFromMille(List<string> servicesList, string services)
        {
            string[] allServices = services.Split('~');

            if (allServices.GetUpperBound(0) > 0)
            {
                string finalServices = "";

                foreach (var service in allServices)
                {
                    if (finalServices.Contains(service))
                    {
                    }
                    else
                    {
                        if (allServices.GetUpperBound(0) == 0)
                        {
                            if (servicesList.Contains(service))
                            {
                                finalServices = finalServices + service;
                            }
                        }
                        else
                        {
                            if (servicesList.Contains(service))
                            {
                                finalServices = finalServices + "~" + service;
                            }
                        }
                    }
                }

                services = finalServices;
            }

            return services;
        }

        private static void ExtractAffiliateMeteData(List<string> servicesList)
        {
            try
            {
                logger.LogInfoMessage("Beginning affiliate document migration at: " + DateTime.Now);

                //int rowCount = 0;
                string affiliateDestinationFileName =  @"C:\temp\TCD IPM Export\Affiliate\AffiliateMetadata.csv";
                string affiliateNoSvcsDestinationFileName = @"C:\temp\TCD IPM Export\Affiliate\AffiliateMetadata_NoSvcs.csv";

                string fileHeader =
                    "Filename|AffiliateId|DocType|DocExpDate|ExpDate|ContractType|Territories|Components|AmendmentId|EffectiveDate|DocSubType|ContractId|DocEffecDate|Services|DocNotes|AffiliateName|DescDate|IPMRecId|IPMCreateDate|Filepath";

                TextWriter affiliateMetadataFile = new StreamWriter(affiliateDestinationFileName, true);
                TextWriter affiliateNoSvcsMetadataFile = new StreamWriter(affiliateNoSvcsDestinationFileName, true);

                affiliateMetadataFile.WriteLine(fileHeader);
                affiliateNoSvcsMetadataFile.WriteLine(fileHeader);

                string exportConnectString =
                    ConfigurationManager.ConnectionStrings["TNSConnectionString"].ConnectionString;

                DataSet ipmExportData = BLL.GetTcdIpmDistinctAffiliateRecords(exportConnectString);

                DataTable ipmExportTable = ipmExportData.Tables[0];

                logger.LogInfoMessage("Successfully completed initial setup for execution at: " +
                                      DateTime.Now);

                foreach (DataRow exportRecord in ipmExportTable.Rows)
                {
                    DataSet tcdRecIdData = BLL.GetTcdIpmRecIdRecord(exportConnectString,
                                                                    exportRecord["RECID"].ToString());

                    DataTable tcdRecIdTable = tcdRecIdData.Tables[0];

                    DataRow tcdRecIdRow = tcdRecIdTable.Rows[0];

                    string exportRecId = tcdRecIdRow["RECID"].ToString();
                    string dbAffiliateId = tcdRecIdRow["AFFILNBR"].ToString();
                    string dbContractId = tcdRecIdRow["MILLE_CONTRACT_ID"].ToString();
                    string metadataRecordData = "|";
                    string affiliateId = "";
                    string documentType = "";
                    string documentExpireDate = "";
                    string expireDate = "";
                    string contractType = "";
                    string territories = "";
                    string components = "";
                    string amendmentId = "";
                    string effectiveDate = "";
                    string documentSubtype = "";
                    string contractId = "";
                    string documentEffectiveDate = "";
                    string services = "";
                    string documentNotes = "";
                    string affiliateName = "";
                    string destructionDate = "";
                    string recId = "";
                    string ipmCreateDate = "";

                    affiliateId = tcdRecIdRow["AFFILNBR"].ToString().Trim();
                    affiliateName = tcdRecIdRow["AFFNAME"].ToString().Trim();
                    documentType = tcdRecIdRow["DOCTYPE"].ToString().ToUpper().Trim();
                    documentSubtype = tcdRecIdRow["DOCSUBTY"].ToString().ToUpper().Trim();
                    effectiveDate = tcdRecIdRow["EFFDATE"].ToString().Trim();
                    expireDate = tcdRecIdRow["EXPDATE"].ToString().Trim();
                    documentEffectiveDate = tcdRecIdRow["EFFDATE"].ToString().Trim();
                    documentExpireDate = tcdRecIdRow["EXPDATE"].ToString().Trim();
                    documentNotes = tcdRecIdRow["DOCNOTES"].ToString().Trim();
                    services = tcdRecIdRow["SERVICE"].ToString().Trim();
                    contractId = tcdRecIdRow["MILLE_CONTRACT_ID"].ToString().Trim();
                    amendmentId = tcdRecIdRow["AMENDID"].ToString().Trim();
                    recId = tcdRecIdRow["RECID"].ToString().Trim();
                    ipmCreateDate = tcdRecIdRow["FILEDDATE"].ToString().Trim();
                    destructionDate = tcdRecIdRow["DESTRUCTION_DATE"].ToString().Trim();

                    //rowCount = rowCount + 1;

                    documentNotes = documentNotes.Replace("'", "''");
                    affiliateName = affiliateName.Replace("'", "''");

                    string tcdAuditSql = "INSERT INTO [TCD_Document_Migration_Audit] " +
                                         "([SourceSystem]" +
                                         ", [AffiliateId]" +
                                         ", [AffiliateName]" +
                                         ", [DocumentType]" +
                                         ", [DocumentSubtype]" +
                                         ", [ContractType]" +
                                         ", [EffectiveDate]" +
                                         ", [ExpireDate]" +
                                         ", [DocumentNotes]" +
                                         ", [Services]" +
                                         ", [Components]" +
                                         ", [Territories]" +
                                         ", [ContractId]" +
                                         ", [AmendmentId]" +
                                         ", [RecId]" +
                                         ", [IpmCreateDate]" +
                                         ", [DestructionDate]" +
                                         ", [RunDate]" +
                                         ", [RunUser]) VALUES ('TCD', '" + affiliateId +
                                         "', '" + affiliateName + "', '" + documentType + "', '" +
                                         documentSubtype + "', '" +
                                         contractType + "', '" + effectiveDate + "', '" + expireDate +
                                         "', '" + documentNotes + "', '" + services + "', '" +
                                         components + "', '" + territories + "', '" + contractId + "', '" +
                                         amendmentId + "', '" + recId + "', '" +
                                         ipmCreateDate + "', '" + destructionDate + "', '" + DateTime.Now +
                                         "', " + "'Audit'),";

                    if (!String.IsNullOrEmpty(dbAffiliateId) && String.IsNullOrEmpty(dbContractId))
                    {
                        try
                        {
                            var returnedData = GetMillenniumData(dbAffiliateId, "Affiliate");

                            if (!String.IsNullOrEmpty(returnedData.AffiliateName))
                            {
                                affiliateName = returnedData.AffiliateName.Trim();
                            }
                            //if (!String.IsNullOrEmpty(effectiveDate))
                            //{
                            //    documentEffectiveDate = effectiveDate.Trim();
                            //}
                            //if (!String.IsNullOrEmpty(expireDate))
                            //{
                            //    documentExpireDate = expireDate.Trim();
                            //}
                        }
                        catch (Exception ex)
                        {
                            logger.LogError(
                                "Mille Contract Summary service failed for affiliate lookup for affiliate id: " +
                                dbAffiliateId);
                            logger.LogException(ex);
                        }
                    }

                    //rowCount = rowCount + 1;

                    documentNotes = documentNotes.Replace("'", "''");
                    affiliateName = affiliateName.Replace("'", "''");

                    string milleAuditSql = "('MILLE', '" + affiliateId + "', '" +
                                           affiliateName + "', '" + documentType + "', '" + documentSubtype +
                                           "', '" +
                                           contractType + "', '" + effectiveDate + "', '" + expireDate +
                                           "', '" + documentNotes + "', '" + services + "', '" +
                                           components + "', '" + territories + "', '" + contractId + "', '" +
                                           amendmentId + "', '" + recId + "', '" +
                                           ipmCreateDate + "', '" + destructionDate + "', '" + DateTime.Now +
                                           "', " + "'Audit'),";

                    services = MassageServices(services);

                    services = ValidateServicesFromMille(servicesList, services);

                    if (!String.IsNullOrEmpty(services) && services.Substring(0, 1) == "~")
                    {
                        services = services.Remove(0, 1);
                    }

                    contractId = contractId.Replace("N/A", "").Trim();

                    if (!String.IsNullOrEmpty(effectiveDate) && effectiveDate == "1/1/1950 12:00:00 AM")
                    {
                        effectiveDate = "";
                    }

                    if (!String.IsNullOrEmpty(expireDate) && expireDate == "1/1/1950 12:00:00 AM")
                    {
                        expireDate = "";
                    }

                    if (!String.IsNullOrEmpty(documentEffectiveDate) && documentEffectiveDate == "1/1/1950 12:00:00 AM")
                    {
                        documentEffectiveDate = "";
                    }

                    if (!String.IsNullOrEmpty(documentExpireDate) && documentExpireDate == "1/1/1950 12:00:00 AM")
                    {
                        documentExpireDate = "";
                    }

                    logger.LogInfoMessage("Successfully set metadata values from MILLE for Rec Id: " +
                                          exportRecId);


                    string pathValue = "";
                    
                    if (!String.IsNullOrEmpty(services))
                    {
                        pathValue = @"\\cnnecmfs\filenetdv\TCD_DOC_Migration\Affiliate\Input\" + recId + ".TIF";
                    }
                    else
                    {
                        pathValue = @"\\cnnecmfs\filenetdv\TCD_DOC_Migration\AffiliateNoSvcs\Input\" + recId + ".TIF";
                    }

                    metadataRecordData = recId + ".TIF" + "|" + affiliateId + "|" + documentType + "|" +
                                         documentExpireDate + "|" + expireDate + "|" + contractType +
                                         "|" +
                                         territories + "|" + components + "|" + amendmentId + "|" +
                                         effectiveDate + "|" + documentSubtype + "|" + contractId + "|" +
                                         documentEffectiveDate + "|" + services + "|" + documentNotes +
                                         "|" +
                                         affiliateName + "|" + destructionDate + "|" + recId + "|" +
                                         ipmCreateDate + "|" + pathValue;


                    //Dividing the records based on presence of services. This is to overcome ICC issue of instering
                    //empty values for multi-value properties when no value is provided.
                    if (!String.IsNullOrEmpty(services))
                    {
                        affiliateMetadataFile.WriteLine(metadataRecordData);
                    }
                    else
                    {
                        affiliateNoSvcsMetadataFile.WriteLine(metadataRecordData);
                    }
                    

                    logger.LogInfoMessage("Successfully wrote metadata record for affiliate for Rec Id: " +
                                          exportRecId);

                    //File.Copy(@"C:\temp\TCD IPM Export\imgD461.TIF", @"C:\temp\TCD IPM Export\Affiliate\" + recId + ".TIF", true);

                    //rowCount = rowCount + 1;

                    documentNotes = documentNotes.Replace("'", "''");
                    affiliateName = affiliateName.Replace("'", "''");

                    string finalAuditSql = "('Final', '" + affiliateId + "', '" +
                                           affiliateName + "', '" + documentType + "', '" + documentSubtype +
                                           "', '" +
                                           contractType + "', '" + effectiveDate + "', '" + expireDate +
                                           "', '" + documentNotes + "', '" + services + "', '" +
                                           components + "', '" + territories + "', '" + contractId + "', '" +
                                           amendmentId + "', '" + recId + "', '" +
                                           ipmCreateDate + "', '" + destructionDate + "', '" + DateTime.Now +
                                           "', " + "'Audit')";

                    tcdAuditSql = tcdAuditSql + milleAuditSql + finalAuditSql;

                    BLL.InsertTcdMigrationAuditRecords(exportConnectString, tcdAuditSql);

                    logger.LogInfoMessage("Successfully inserted audit records for Rec Id: " + exportRecId);
                }

                affiliateMetadataFile.Close();
                affiliateNoSvcsMetadataFile.Close();

                logger.LogInfoMessage("Ending affiliate document migration at: " + DateTime.Now);
            }
            catch (Exception ex)
            {
                logger.LogError("Affiliate export failed.");
                logger.LogException(ex);
            }
        }

        private static void ExtractBizDevMetaData(List<string> servicesList)
        {
            try
            {
                logger.LogInfoMessage("Beginning business development document migration at: " +
                                      DateTime.Now);

                //int rowCount = 0;
                string busdevDestinationFileName = @"C:\temp\TCD IPM Export\BusinessDevelopment\BusinessDevelopmentMetadata.csv";
                string busdevNoSvcsDestinationFileName = @"C:\temp\TCD IPM Export\BusinessDevelopment\BusinessDevelopmentNoSvcsMetadata.csv";

                string fileHeader =
                    "Filename|AffiliateId|DocType|DocExpDate|ExpDate|ContractType|Territories|Components|AmendmentId|EffectiveDate|DocSubType|ContractId|DocEffecDate|Services|DocNotes|AffiliateName|DescDate|IPMRecId|IPMCreateDate|Filepath";

                TextWriter busdevMetadataFile = new StreamWriter(busdevDestinationFileName, true);
                TextWriter busdevNoSvcsMetadataFile = new StreamWriter(busdevNoSvcsDestinationFileName, true);

                busdevMetadataFile.WriteLine(fileHeader);
                busdevNoSvcsMetadataFile.WriteLine(fileHeader);

                string exportConnectString =
                    ConfigurationManager.ConnectionStrings["TNSConnectionString"].ConnectionString;

                DataSet ipmExportData = BLL.GetTcdIpmDistinctBusDevRecords(exportConnectString);

                DataTable ipmExportTable = ipmExportData.Tables[0];

                logger.LogInfoMessage("Successfully completed initial setup for execution at: " +
                                      DateTime.Now);

                foreach (DataRow exportRecord in ipmExportTable.Rows)
                {
                    DataSet tcdRecIdData = BLL.GetTcdIpmBusDevRecord(exportConnectString,
                                                                     exportRecord["RECID"].ToString());

                    DataTable tcdRecIdTable = tcdRecIdData.Tables[0];

                    DataRow tcdRecIdRow = tcdRecIdTable.Rows[0];

                    string exportRecId = tcdRecIdRow["RECID"].ToString();
                    string dbAffiliateId = tcdRecIdRow["AFFILNBR"].ToString();
                    string dbContractId = tcdRecIdRow["MILLE_CONTRACT_ID"].ToString();
                    string metadataRecordData = "|";
                    string affiliateId = "";
                    string documentType = "";
                    string documentExpireDate = "";
                    string expireDate = "";
                    string contractType = "";
                    string territories = "";
                    string components = "";
                    string amendmentId = "";
                    string effectiveDate = "";
                    string documentSubtype = "";
                    string contractId = "";
                    string documentEffectiveDate = "";
                    string services = "";
                    string documentNotes = "";
                    string affiliateName = "";
                    string destructionDate = "";
                    string recId = "";
                    string ipmCreateDate = "";

                    affiliateId = tcdRecIdRow["AFFILNBR"].ToString().Trim();
                    affiliateName = tcdRecIdRow["AFFNAME"].ToString().Trim();
                    documentType = tcdRecIdRow["DOCTYPE"].ToString().ToUpper().Trim();
                    documentSubtype = tcdRecIdRow["DOCSUBTY"].ToString().ToUpper().Trim();
                    effectiveDate = tcdRecIdRow["EFFDATE"].ToString().Trim();
                    expireDate = tcdRecIdRow["EXPDATE"].ToString().Trim();
                    documentEffectiveDate = tcdRecIdRow["EFFDATE"].ToString().Trim();
                    documentExpireDate = tcdRecIdRow["EXPDATE"].ToString().Trim();
                    documentNotes = tcdRecIdRow["DOCNOTES"].ToString().Trim();
                    services = tcdRecIdRow["SERVICE"].ToString().Trim();
                    contractId = tcdRecIdRow["MILLE_CONTRACT_ID"].ToString().Trim();
                    amendmentId = tcdRecIdRow["AMENDID"].ToString().Trim();
                    recId = tcdRecIdRow["RECID"].ToString().Trim();
                    ipmCreateDate = tcdRecIdRow["FILEDDATE"].ToString().Trim();
                    destructionDate = tcdRecIdRow["DESTRUCTION_DATE"].ToString().Trim();

                    //rowCount = rowCount + 1;

                    documentNotes = documentNotes.Replace("'", "''");
                    affiliateName = affiliateName.Replace("'", "''");

                    string tcdAuditSql = "INSERT INTO [TCD_Document_Migration_Audit] " +
                                         "([SourceSystem]" +
                                         ", [AffiliateId]" +
                                         ", [AffiliateName]" +
                                         ", [DocumentType]" +
                                         ", [DocumentSubtype]" +
                                         ", [ContractType]" +
                                         ", [EffectiveDate]" +
                                         ", [ExpireDate]" +
                                         ", [DocumentNotes]" +
                                         ", [Services]" +
                                         ", [Components]" +
                                         ", [Territories]" +
                                         ", [ContractId]" +
                                         ", [AmendmentId]" +
                                         ", [RecId]" +
                                         ", [IpmCreateDate]" +
                                         ", [DestructionDate]" +
                                         ", [RunDate]" +
                                         ", [RunUser]) VALUES ('TCD', '" + affiliateId +
                                         "', '" + affiliateName + "', '" + documentType + "', '" +
                                         documentSubtype + "', '" +
                                         contractType + "', '" + effectiveDate + "', '" + expireDate +
                                         "', '" + documentNotes + "', '" + services + "', '" +
                                         components + "', '" + territories + "', '" + contractId + "', '" +
                                         amendmentId + "', '" + recId + "', '" +
                                         ipmCreateDate + "', '" + destructionDate + "', '" + DateTime.Now +
                                         "', " + "'Audit'),";

                    //rowCount = rowCount + 1;

                    documentNotes = documentNotes.Replace("'", "''");
                    affiliateName = affiliateName.Replace("'", "''");

                    string milleAuditSql = "('MILLE', '" + affiliateId + "', '" +
                                           affiliateName + "', '" + documentType + "', '" + documentSubtype +
                                           "', '" +
                                           contractType + "', '" + effectiveDate + "', '" + expireDate +
                                           "', '" + documentNotes + "', '" + services + "', '" +
                                           components + "', '" + territories + "', '" + contractId + "', '" +
                                           amendmentId + "', '" + recId + "', '" +
                                           ipmCreateDate + "', '" + destructionDate + "', '" + DateTime.Now +
                                           "', " + "'Audit'),";

                    services = MassageServices(services);

                    services = ValidateServicesFromMille(servicesList, services);

                    if (!String.IsNullOrEmpty(services) && services.Substring(0, 1) == "~")
                    {
                        services = services.Remove(0, 1);
                    }

                    contractId = contractId.Replace("N/A", "").Trim();

                    if (!String.IsNullOrEmpty(effectiveDate) && effectiveDate == "1/1/1950 12:00:00 AM")
                    {
                        effectiveDate = "";
                    }

                    if (!String.IsNullOrEmpty(expireDate) && expireDate == "1/1/1950 12:00:00 AM")
                    {
                        expireDate = "";
                    }

                    if (!String.IsNullOrEmpty(documentEffectiveDate) && documentEffectiveDate == "1/1/1950 12:00:00 AM")
                    {
                        documentEffectiveDate = "";
                    }

                    if (!String.IsNullOrEmpty(documentExpireDate) && documentExpireDate == "1/1/1950 12:00:00 AM")
                    {
                        documentExpireDate = "";
                    }

                    logger.LogInfoMessage("Successfully set metadata values from MILLE for Rec Id: " +
                                          exportRecId);

                    string pathValue = "";

                    if (!String.IsNullOrEmpty(services))
                    {
                        pathValue = @"\\cnnecmfs\filenetdv\TCD_DOC_Migration\Affiliate\Input\" + recId + ".TIF";
                    }
                    else
                    {
                        pathValue = @"\\cnnecmfs\filenetdv\TCD_DOC_Migration\AffiliateNoSvcs\Input\" + recId + ".TIF";
                    }
                    
                    metadataRecordData = recId + ".TIF" + "|" + affiliateId + "|" + documentType + "|" +
                                         documentExpireDate + "|" + expireDate + "|" + contractType +
                                         "|" +
                                         territories + "|" + components + "|" + amendmentId + "|" +
                                         effectiveDate + "|" + documentSubtype + "|" + contractId + "|" +
                                         documentEffectiveDate + "|" + services + "|" + documentNotes +
                                         "|" +
                                         affiliateName + "|" + destructionDate + "|" + recId + "|" +
                                         ipmCreateDate + "|" + pathValue;

                    //Dividing the records based on presence of services. This is to overcome ICC issue of instering
                    //empty values for multi-value properties when no value is provided.
                    if (!String.IsNullOrEmpty(services))
                    {
                        busdevMetadataFile.WriteLine(metadataRecordData);
                    }
                    else
                    {
                        busdevNoSvcsMetadataFile.WriteLine(metadataRecordData);
                    }
                    

                    logger.LogInfoMessage(
                        "Successfully wrote metadata record for business development for Rec Id: " +
                        exportRecId);

                    //File.Copy(@"C:\temp\TCD IPM Export\imgD461.TIF", @"C:\temp\TCD IPM Export\BusinessDevelopment\" + recId + ".TIF", true);

                    //rowCount = rowCount + 1;

                    documentNotes = documentNotes.Replace("'", "''");
                    affiliateName = affiliateName.Replace("'", "''");

                    string finalAuditSql = "('Final', '" + affiliateId + "', '" +
                                           affiliateName + "', '" + documentType + "', '" + documentSubtype +
                                           "', '" +
                                           contractType + "', '" + effectiveDate + "', '" + expireDate +
                                           "', '" + documentNotes + "', '" + services + "', '" +
                                           components + "', '" + territories + "', '" + contractId + "', '" +
                                           amendmentId + "', '" + recId + "', '" +
                                           ipmCreateDate + "', '" + destructionDate + "', '" + DateTime.Now +
                                           "', " + "'Audit')";

                    tcdAuditSql = tcdAuditSql + milleAuditSql + finalAuditSql;

                    BLL.InsertTcdMigrationAuditRecords(exportConnectString, tcdAuditSql);

                    logger.LogInfoMessage("Successfully inserted audit records for Rec Id: " + exportRecId);
                }

                busdevMetadataFile.Close();
                busdevNoSvcsMetadataFile.Close();

                logger.LogInfoMessage("Ending affiliate document migration at: " + DateTime.Now);
            }
            catch (Exception ex)
            {
                logger.LogError("Affiliate export failed.");
                logger.LogException(ex);
            }
        }

        private static void ExtractImagesFromIPM()
        {
            try
            {
                logger.LogInfoMessage("Beginning TCD IPM document export at: " + DateTime.Now);

                var userClass = new User();

                var userToken = userClass.Login("optika", "p4=fvYwv", false);

                string searchName = "TNS_RECID";

                NamedQueries namedQueries = new NamedQueries();

                namedQueries.UserToken = userToken;
                namedQueries.Refresh();

                NamedQuery namedQuery = namedQueries[searchName];

                string exportConnectString =
                    ConfigurationManager.ConnectionStrings["TNSConnectionString"].ConnectionString;

                DataSet ipmExportData = BLL.GetTcdIpmDistinctContentContractRecords(exportConnectString);
                //DataSet ipmExportData = BLL.GetTcdIpmDistinctAffiliateContentRecords(exportConnectString);
                //DataSet ipmExportData = BLL.GetTcdIpmDistinctBusDevRecords(exportConnectString);
                //DataSet ipmExportData = BLL.GetTcdIpmRecIdPageCountRecord(exportConnectString);

                DataTable ipmExportTable = ipmExportData.Tables[0];

                logger.LogInfoMessage("Successfully completed initial setup for execution at: " +
                                      DateTime.Now);

                int rowCount = ipmExportTable.Rows.Count;

                foreach (DataRow exportRecord in ipmExportTable.Rows)
                {
                    string exportRecId = exportRecord["RECID"].ToString();
                    //string exportRecId = "1465477502";

                    namedQuery.Conditions[1].RightValue = exportRecId;

                    QueryConnectionManager queryConnectionManager = new QueryConnectionManager();

                    queryConnectionManager.UserToken = userToken;

                    QueryConnection queryConnection = queryConnectionManager.CreateConnection();

                    queryConnection.ExecuteQuery(namedQuery, RecordsetManagement.rsmUnmanaged, 1);

                    var objRecordSet = new Recordset();

                    objRecordSet = queryConnection.GetResults(RecordsetReturn.rsrAll).Recordset as Recordset;

                    var objDocInfo = new DocInfo();

                    objDocInfo.ADOFields = objRecordSet.Fields;

                    queryConnectionManager.DestroyConnection(queryConnection.ConnectionId);

                    ExportedDocument objExportedDocument;
                    int pageCount;

                    PrepareIPMDocExportEnv(userToken, objDocInfo, out objExportedDocument, out pageCount);

                    string objExportedPath = objExportedDocument.RecombineDocument(1, pageCount).FullPath;
                    //string objExportedPath = objExportedDocument.Pages[1].Sections[1].FullPath;

                    logger.LogInfoMessage("Successfully exported content file for Rec Id: " + exportRecId);

                    string exportedFileName = Path.GetFileName(objExportedPath);
                    //string destinationFilePath = @"\\ipmvfiler\DMSFS\temp\TCDDocMigration\IPM Contract Content Files\" + exportRecId + ".TIF";
                    //string destinationFilePath = @"\\ipmvfiler\DMSFS\temp\TCDDocMigration\IPM Affiliate Content Files\" + exportRecId + ".TIF";
                    //string destinationFilePath = @"\\ipmvfiler\DMSFS\temp\TCDDocMigration\IPM Bus Dev Content Files\" + exportRecId + ".TIF";

                    string destinationFilePath = @"\\ipmvfiler\DMSFS\temp\TCDDocMigration\Deepak - Contract Quality Content\" + exportRecId + ".TIF";
                    //string destinationFilePath = @"\\ipmvfiler\DMSFS\temp\TCDDocMigration\Deepak - Affiliate Quality Content\" + exportRecId + ".TIF";
                    //string destinationFilePath = @"\\ipmvfiler\DMSFS\temp\TCDDocMigration\Deepak - BizDev Quality Content\" + exportRecId + ".TIF";

                    File.Copy(objExportedPath, destinationFilePath, true);

                    logger.LogInfoMessage("Successfully copied content file for Rec Id: " + exportRecId);
                }

                userToken.Logout();

                logger.LogInfoMessage("Ending TCD IPM document export at: " + DateTime.Now);
            }
            catch (Exception ex)
            {
                logger.LogError("TCD IPM file export failed.");
                logger.LogException(ex);
            }
        }

        /*private static void PrepareFarmIPMDocExportEnv(UserToken userToken, DocInfo objDocInfo, out ExportedDocument objExportedDocument, out int pageCount)
        {
            objExportedDocument = new ExportedDocument();
            var objExportEnvironment = new ExportEnvironment();

            objExportEnvironment.ExportBasePath = @"C:\temp\FARM";
            objExportEnvironment.ExportNameSpace = "IPMDocumentExport";
            //objExportEnvironment.ExportType = ExportFileType.etTIF;
            objExportEnvironment.ExportType = ExportFileType.etNative;
            objExportEnvironment.ExportDPI = 300;
            objExportEnvironment.ExportScale = 300;
            
            
            objExportedDocument.UserToken = userToken;
            objExportedDocument.ExportEnvironment = objExportEnvironment;
            objExportedDocument.ExportEnvironment.MaxOutputPages = 199;
            objExportedDocument.DocInfo = objDocInfo;

            pageCount = objExportedDocument.Pages.Count;
        }*/

        private static void PrepareFarmIPMDocExportEnv(UserToken userToken, DocInfo objDocInfo, int page, out ExportedDocument objExportedDocument, out int pageCount)
        {
            objExportedDocument = new ExportedDocument();
            var objExportEnvironment = new ExportEnvironment();

            objExportEnvironment.ExportBasePath = @"C:\temp\FARM";
            objExportEnvironment.ExportNameSpace = "IPMDocumentExport";

            if (page == 1)
            {
                objExportEnvironment.ExportType = ExportFileType.etNative;
                objExportEnvironment.ExportScale = 300;
            }
            else
            {
                objExportEnvironment.ExportType = ExportFileType.etTIF;
                objExportEnvironment.ExportScale = 200;
            }
            //objExportEnvironment.ExportType = ExportFileType.etTIF;
           
            objExportedDocument.UserToken = userToken;
            objExportedDocument.ExportEnvironment = objExportEnvironment;
            objExportedDocument.ExportEnvironment.MaxOutputPages = 199;
            objExportedDocument.DocInfo = objDocInfo;

            pageCount = objExportedDocument.Pages.Count;
        }

        private static void PrepareIPMDocExportEnv(UserToken userToken, DocInfo objDocInfo, out ExportedDocument objExportedDocument, out int pageCount)
        {
            objExportedDocument = new ExportedDocument();
            var objExportEnvironment = new ExportEnvironment();

            objExportEnvironment.ExportBasePath = @"C:\temp\Int_Sales_Export";
            objExportEnvironment.ExportNameSpace = "IPMDocumentExport";
            //objExportEnvironment.ExportType = ExportFileType.etTIF;
            objExportEnvironment.ExportType = ExportFileType.etNative;
            objExportEnvironment.ExportDPI = 300;
            objExportEnvironment.ExportScale = 140;

            objExportedDocument.UserToken = userToken;
            objExportedDocument.ExportEnvironment = objExportEnvironment;
            objExportedDocument.ExportEnvironment.MaxOutputPages = 199;
            objExportedDocument.DocInfo = objDocInfo;

            pageCount = objExportedDocument.Pages.Count;
        }

        private static void IPMDocExportEnv(UserToken userToken, DocInfo objDocInfo, out ExportedDocument objExportedDocument, out int pageCount)
        {
            objExportedDocument = new ExportedDocument();
            var objExportEnvironment = new ExportEnvironment();

            objExportEnvironment.ExportBasePath = @"C:\temp\FARM";
            objExportEnvironment.ExportNameSpace = "IPMDocumentExport";
            //objExportEnvironment.ExportType = ExportFileType.etTIF;
            objExportEnvironment.ExportType = ExportFileType.etNative;
            objExportEnvironment.ExportDPI = 300;
            objExportEnvironment.ExportScale = 140;

            objExportedDocument.UserToken = userToken;
            objExportedDocument.ExportEnvironment = objExportEnvironment;
            objExportedDocument.ExportEnvironment.MaxOutputPages = 199;
            objExportedDocument.DocInfo = objDocInfo;

            pageCount = objExportedDocument.Pages.Count;
        }
        private static string ExtractFarmAttachmentContent(string SourceFileName, string OutputFileName, string PKGId)
        {

            string strfileName = OutputFileName.Replace(' ', '_').Replace(',', '-') + "_" + "_" + Path.GetFileName(SourceFileName).Replace(' ', '_');
            string Sourcefilepath = "";
            string strPkgId = PKGId.Trim();
            //string strSupportingFilePath = ConfigurationManager.AppSettings["FilePath_IPMSupportingDoc"]; // Convert.ToString(ConfigurationSettings.AppSettings["FilePath_IPMSupportingDoc"]);
            string strSupportingFilePath = @"C:\temp\FARM\Attachments\";

            try
            {
                FileStream stream = File.OpenRead(SourceFileName);
                byte[] fileBytes = new byte[stream.Length];

                stream.Read(fileBytes, 0, fileBytes.Length);
                stream.Close();
                //Begins the process of writing the byte array back to a file

                using (System.IO.Stream file = File.OpenWrite(strSupportingFilePath + strfileName))
                {
                    file.Write(fileBytes, 0, fileBytes.Length);
                }

                Sourcefilepath = strSupportingFilePath + strfileName;

            }
            catch (Exception ex)
            {
                logger.LogError("Error extracting the attachment document for PKGID: " + strPkgId);
                throw ex;
            }

            return Sourcefilepath;
        }
        private static string ExtractAttachmentContent(string SourceFileName, string OutputFileName, string ExpYear, string PKGId, int rowCount)
        {

            string strfileName = OutputFileName.Replace(' ', '_').Replace(',','-') + "_" + rowCount.ToString() + "_" + Path.GetFileName(SourceFileName).Replace(' ','_');
            string Sourcefilepath = "";
            string strPkgId = PKGId.Trim();
            //string strSupportingFilePath = ConfigurationManager.AppSettings["FilePath_IPMSupportingDoc"]; // Convert.ToString(ConfigurationSettings.AppSettings["FilePath_IPMSupportingDoc"]);
            string strSupportingFilePath = @"C:\temp\Int_Sales_Export\20" + ExpYear + "\\";

            try
            {
                FileStream stream = File.OpenRead(SourceFileName);
                byte[] fileBytes = new byte[stream.Length];

                stream.Read(fileBytes, 0, fileBytes.Length);
                stream.Close();
                //Begins the process of writing the byte array back to a file

                using (System.IO.Stream file = File.OpenWrite(strSupportingFilePath + strfileName))
                {
                    file.Write(fileBytes, 0, fileBytes.Length);
                }

                Sourcefilepath = strSupportingFilePath + strfileName;
                
            }
            catch (Exception ex)
            {
                logger.LogError("Error extracting the attachment document for PKGID: " + strPkgId);
                throw ex;
            }

            return Sourcefilepath;
        }

            

        private static string MassageServices(string services)
        {
            string origServices = services;
            //Strip 'content' first from the end of services
            services = services.Replace(" Content", "").Trim();

            services = services.Replace("Turner Sports Sheet", "TS").Trim();
            services = services.Replace("Adult Swim", "TOON").Trim();

            services = services.Replace("+", "").Trim();
            services = services.Replace(" ", "~").Trim();
            services = services.Replace(",", "~").Trim();
            services = services.Replace("/", "~").Trim();            
            services = services.Replace("&", "~").Trim();

            services = services.Replace("TOONS", "TOON").Trim();            
            services = services.Replace("INfinito", "INF").Trim();
            services = services.Replace("Boomerang", "BOOM").Trim();
            services = services.Replace("Nascar", "TNT").Trim();
            services = services.Replace("PGA", "TNT").Trim();
            services = services.Replace("GameTap", "TNT").Trim();
            services = services.Replace("HN", "HLN").Trim();
            services = services.Replace("CTV", "TruTV").Trim();
            services = services.Replace("Court", "TruTV").Trim();
            services = services.Replace("CNNi", "CNNI").Trim();
            services = services.Replace("CNNe", "CNNE").Trim();
            services = services.Replace("N/A", "").Trim();            
            services = services.Replace("Multi Service", "").Trim();
            services = services.Replace("Multi-Service", "").Trim();

            return services;
        }

        private static ContractSummaryDc GetMillenniumData(string idValue, string idType)
        {
            var request = new GetContractSummaryRequestMessage();

            request.Criteria = new ContractSummaryCriteriaDC();

            if (idType == "Contract")
            {
                request.Criteria.ContractId = Convert.ToInt32(idValue);
            }
            else if (idType == "Affiliate")
            {
                request.Criteria.AffiliateId = Convert.ToInt32(idValue);
            }

            GetContractSummaryResponseMessage response = null;

            Execute(client => response = client.GetContractSummary(request));

            var returnedData = response.ContractInfo;

            return returnedData;
        }

        protected static void Execute(Action<ContractServiceClient> executeMethod)
        {
            using (var client = new ServiceClient<ContractServiceClient, IContractService>())
            {
                executeMethod(client.Proxy);
            }
        }
    }
}
