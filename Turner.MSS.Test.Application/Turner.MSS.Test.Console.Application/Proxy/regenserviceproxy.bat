﻿if exist "C:\Program Files\Microsoft SDKs\Windows\v7.0A\Bin\svcutil.exe" (
	SET cmd="C:\Program Files\Microsoft SDKs\Windows\v7.0A\Bin\svcutil.exe"
) else (
	SET cmd="C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Bin\NETFX 4.0 Tools\svcutil.exe"
)

SET cmd=%cmd% /mc /edb  /out:MillenniumServiceProxy 


REM SET cmd=%cmd% http://localhost:1841/MillenniumServices/AffiliateServiceProxy.svc?wsdl
REM SET cmd=%cmd% http://localhost:1841/MillenniumServices/ContractServiceProxy.svc?wsdl


REM ******** uncoment this to build the proxy from CI
REM SET cmd=%cmd% http://affiliateportalinternal.turner.com/milleservice/dev/AffiliateServiceProxy.svc?wsdl

REM SET cmd=%cmd% http://affiliateportalinternal.turner.com/milleservice/uat/ContractServiceProxy.svc?wsdl

SET cmd=%cmd% http://affiliateportalinternal.turner.com/milleservice/dev/ContractServiceProxy.svc?wsdl

REM SET cmd=%cmd% http://affiliateportalinternal.turner.com/milleservice/qa/ContractServiceProxy.svc?wsdl

REM SET cmd=%cmd% http://otmqa.turner.com/Services/ObligationHeaderService.svc?wsdl

%cmd%
pause