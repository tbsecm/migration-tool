using System;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using Turner.MSS.Logging;

namespace Turner.MSS.Test.Console.Application
{
    public class UserRole
    {
        private string _userType;
        private int _userGroup;
        private int _userDivision;
        private WindowsIdentity _winId = WindowsIdentity.GetCurrent();
        private WindowsPrincipal _userPrincipal;
        private GenericPrincipal _userGenericPrincipal;
        private GenericIdentity _userGenericIdentity;
        private ILogger _logger = new Logger();

        public UserRole()
        {
            CreateUserRole();
        }

        public string UserType
        {
            get { return _userType; }
        }

        public int UserGroup
        {
            get { return _userGroup; }
        }

        public int UserDivision
        {
            get { return _userDivision; }
        }

        public WindowsPrincipal UserPrincipal
        {
            get { return _userPrincipal; }
        }

        public GenericPrincipal UserGenericPrincipal
        {
            get { return _userGenericPrincipal; }
        }

        public GenericIdentity UserGenericIdentity
        {
            get { return _userGenericIdentity; }
        }

        public void CreateUserRole()
        {
            // Extract the forms authentication cookie
            string cookieName = FormsAuthentication.FormsCookieName;
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[cookieName];

            if (null == authCookie)
            {
                // There is no authentication cookie.
                _logger.LogError("CRD authentication cookie is missing.");

                return;
            }

            // The following code extracts and decrypts the FormsAuthenticationTicket from the cookie. 
            FormsAuthenticationTicket authTicket = null;

            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            }
            catch (Exception ex)
            {
                // Log exception details (omitted for simplicity)
                _logger.LogError("CRD authentication cookie values failed retrieval.");
                _logger.LogException(ex);

                //return false;
            }

            if (null == authTicket)
            {
                // Cookie failed to decrypt.
                _logger.LogError("CRD authentication cookie failed decryption.");

                return;
            }

            // The following code to parse out the pipe separate list of group names attached to the ticket when the user was originally authenticated. 
            if (authTicket != null)
            {
                String[] groups = null;

                //groups = authTicket.UserData.Split(new char[] { '|' });
                //groups = authCookie.Values["groups"].Split(new char[] { '|' });

                // The following code creates a GenericIdentity object with the user name obtained from the ticket name and a GenericPrincipal object that contains this identity together with the user's group list. 
                _userGenericIdentity = new GenericIdentity(authTicket.Name,
                                                          "LdapAuthentication");

                // This principal will flow throughout the request.
                _userGenericPrincipal = new GenericPrincipal(_userGenericIdentity, groups);


                _userPrincipal = new WindowsPrincipal(_winId);

                //var loginUser = new COPS_USER_SECURITY();

                //try
                //{
                //    loginUser =
                //        CRDServiceGateway.UserSecurityService.GetUserSecurityById(_userGenericPrincipal.Identity.Name);

                //    _userType = loginUser.USER_TYPE;
                //    _userGroup = (int) loginUser.GROUP_ID;
                //    _userDivision = (int) loginUser.DIV_ID;
                //}
                //catch (Exception ex)
                //{
                //    _logger.LogError("GetUserSecurityById: NT Id=" + _userGenericPrincipal.Identity.Name);
                //    _logger.LogException(ex);

                //    return;
                //}
            }
        }
    }
}

