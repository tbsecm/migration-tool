﻿using System;
using System.ServiceModel.Configuration;

namespace Turner.MSS.Test.Console.Application.Client.Behavior
{
    public class ClientContextBehaviorSection : BehaviorExtensionElement
    {
        /// <summary>
        /// Gets the type of the behavior
        /// </summary>
        public override Type BehaviorType
        {
            get { return typeof(ClientContextBehavior); }
        }

        /// <summary>
        /// Creates an instance of the ClietnMilleContextBehavior
        /// </summary>
        /// <returns></returns>
        protected override object CreateBehavior()
        {
            return new ClientContextBehavior();
        }
    }
}
