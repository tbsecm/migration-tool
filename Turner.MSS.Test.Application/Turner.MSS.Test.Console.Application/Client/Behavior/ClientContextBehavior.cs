﻿using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using Turner.MSS.Logging;

namespace Turner.MSS.Test.Console.Application.Client.Behavior
{
    public class ClientContextBehavior : IEndpointBehavior, IClientMessageInspector
    {
        private const string MILLENNIUM_SCHEMA_NAMESPACE = "http://schema.turner.com/ea/millennium/2008/06";
        private const string AUTHENTICATION_HEADER_NAME = "ProxyAuthorization";

        //private static readonly ILog _logger = LogManager.GetLogger(typeof(ClientContextBehavior));
        protected static ILogger logger = new Logger();


        #region IEndpointBehavior Members

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            //if (_logger.IsDebugEnabled)
            //    _logger.Debug("Adding message inspector to client runtime.");

            logger.LogInfoMessage("Adding message inspector to client runtime.");

            clientRuntime.MessageInspectors.Add(this);
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        #endregion

        #region IClientMessageInspector Members

        [DebuggerStepThrough]
        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
        }

        public Message testmessage { get; set; }

        [DebuggerStepThrough]
        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            //if (_logger.IsDebugEnabled)
            //    _logger.Debug("Preparing send request.");

            logger.LogInfoMessage("Preparing send request.");

            var headers = request.Headers;
            AddCredentials(headers, request);

            testmessage = request;
            return null;
        }

        private static void AddCredentials(MessageHeaders headers, Message request)
        {
            var index = headers.FindHeader(AUTHENTICATION_HEADER_NAME, MILLENNIUM_SCHEMA_NAMESPACE);

            IUserInformation header = null;
            if (index >= 0)
            {
                header = headers.GetHeader<IUserInformation>(index);

                //if (_logger.IsDebugEnabled)
                //    _logger.Debug("Removing existing header for Authentication.");

                logger.LogInfoMessage("Removing existing header for Authentication.");

                headers.RemoveAt(index);
            }
            if (header == null)
            {
                header = new UserInformation ();
            }

            var newHeader = new MessageHeader<IUserInformation>(header);

            //if (_logger.IsDebugEnabled)
            //    _logger.Debug("Adding header for Authentication.");

            logger.LogInfoMessage("Adding header for Authentication.");

            headers.Add(newHeader.GetUntypedHeader(AUTHENTICATION_HEADER_NAME, MILLENNIUM_SCHEMA_NAMESPACE));
        }

        #endregion
    }

    public interface IUserInformation
    {
        string Domain { get; set; }
        string Username { get; set; }
        string Password { get; set; }
    }

    public class UserInformation : IUserInformation
    {
        public string Domain { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
