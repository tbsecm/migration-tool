﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using Turner.MSS.Logging;

namespace Turner.MSS.Test.Console.Application.Client
{
    public class ServiceClient
    {
        //protected static readonly ILog _logger = LogManager.GetLogger("ServiceClient");
        protected static ILogger logger = new Logger();

        protected ServiceClient()
        {
        }
    }

    public sealed class ServiceClient<TProxy, TChannel> : ServiceClient, IDisposable
        where TProxy : ClientBase<TChannel>, new()
        where TChannel : class
    {
        /// <summary>
        /// variable to hold proxy instance
        /// </summary>
        private TProxy _proxy;


        /// <summary>
        /// Gets the WCF service proxy wrapped by this instance.
        /// </summary>
        public TProxy Proxy
        {
            get
            {
                if (_proxy != null)
                    return _proxy;

                throw new ObjectDisposedException("ServiceClient");
            }
        }

        /// <summary>
        /// default constructor.
        /// </summary>
        public ServiceClient()
        {

                _proxy = new TProxy();

                // this routine swaps the domain of the address based upon the user specified environment
                _proxy.Endpoint.Address = new EndpointAddress(new Uri(ServiceUrl));
        }

        
        private string ServiceUrl
        {
            get
            {
                if (_proxy == null)
                    return string.Empty;

                var path = _proxy.Endpoint.Address.Uri.PathAndQuery;
                var serviceName = path.Substring(path.LastIndexOf("/") + 1);
                var serviceDomain = System.Configuration.ConfigurationManager.AppSettings["ServiceDomain"];
                return string.Format("{0}/{1}", serviceDomain, serviceName);
            }
        }

        public event CancelEventHandler ProxyAborting;
        public event EventHandler ProxyAborted;

        private void AbortConnection()
        {
            if (_proxy == null) return;

            var cancelEventArgs = new CancelEventArgs();
            if (ProxyAborting != null)
                ProxyAborting(this, cancelEventArgs);

            if (cancelEventArgs.Cancel) return;

            _proxy.Abort();
            if (ProxyAborted != null) ProxyAborted(this, new EventArgs());
        }

        public void CloseConnection()
        {
            try
            {
                if (_proxy == null) return;

                if (_proxy.State != CommunicationState.Faulted)
                    _proxy.Close();
                else
                    AbortConnection();
            }
            catch (FaultException unknownFault)
            {
                //if (_logger.IsDebugEnabled)
                //    _logger.DebugFormat("An unknown exception was received. {0}", unknownFault.Message);

                logger.LogError("Millennium Service failed with unknown exception:");
                logger.LogException(unknownFault);

                AbortConnection();
            }
            catch (CommunicationException ex)
            {
                //if (_logger.IsErrorEnabled)
                //    _logger.Error("Service proxy encountered a communication exception and aborted:  {0}", ex);

                logger.LogError("Millennium Service failed with communication exception:");
                logger.LogException(ex);

                AbortConnection();
            }
            catch (TimeoutException ex)
            {
                //if (_logger.IsErrorEnabled)
                //    _logger.ErrorFormat("Service proxy encountred a timeout exception and aborted:  {0}", ex);

                logger.LogError("Millennium Service failed with timeout exception:");
                logger.LogException(ex);

                AbortConnection();
            }
            catch (Exception ex)
            {
                //if (_logger.IsErrorEnabled)
                //    _logger.ErrorFormat("Service proxy encountred a unknown exception and aborted:  {0}", ex);

                logger.LogError("Millennium Service failed with general exception:");
                logger.LogException(ex);

                AbortConnection();
                throw;
            }
        }

        /// <summary>
        /// Disposes the current instance.
        /// </summary>
        [DebuggerStepThrough]
        public void Dispose()
        {
            try
            {
                CloseConnection();
            }
            finally
            {
                _proxy = null;
            }
        }
    }

}
