﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Reflection;
using Turner.MSS.Logging;
using log4net;
using log4net.Config;

namespace Turner.MSS.Test.Data
{
    public class BLL
    {
        private static ILogger logger = new Logger();

        //private static Logging.ILogger logger = new Logging.Logger();
        //private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //XmlConfigurator.Configure(new FileInfo("log4net.config"));

        public class RemedyTicketInformation
        {
            public string RemedyTicketDefinitionId { get; set; }
            public string RemedyTicketNumber { get; set; }
            public string Type { get; set; }
            public string SubType { get; set; }
            public bool IsActive { get; set; }
            public DateTime CreatedDate { get; set; }
            public string CreatedBy { get; set; }
            public DateTime ModifiedDate { get; set; }
            public string ModifiedBy { get; set; }
        }

        public static void ExecuteSQLQuery(string strConnectionString)
        {
            try
            {
                string strOracleSQLStatement;

                strOracleSQLStatement =
                    "SELECT b.PKGID, b.DESCRIPTION, b.UNIQUEID FROM WFUSER.WF_FIELDVALUE_7 a, PROCESS.WF_PKGATTACHMENT b, PROCESS.WF_PKGDEFINITION c WHERE a.PKGID = b.PKGID AND a.PKGID = c.PKGID AND b.PKGID = c.PKGID";

                DataSet dsOracleRecords = new DataSet();

                dsOracleRecords = DAL.GetOracleDataSet(strConnectionString, strOracleSQLStatement);

                foreach (DataRow oracleRow in dsOracleRecords.Tables[0].Rows)
                {
                    //File.Copy() 
                    //oracleRow["batch_id"]

                    string filePath = oracleRow["UNIQUEID"].ToString();
                    string fileName = Path.GetFileName(filePath);
                    //string sourcePath = @"C:\Users\Public\TestFolder";
                    string sourcePath = "@" + filePath;
                    string sourceFileName = @filePath;
                    string destinationPath = @"\\eafilesvfiler\projects\Corporate Finance Documents\" + oracleRow["PKGID"].ToString();
                    string destinationFileName = Path.Combine(destinationPath, fileName);

                    //File.Copy(sourceFileName, destinationFileName, true);

                    try
                    {
                        if (!Directory.Exists(destinationPath))
                        {
                            Directory.CreateDirectory(destinationPath);

                            try
                            {
                                string metaDataFileName = @"MetaDataFile" + "-" + oracleRow["PKGID"].ToString() + ".csv";
                                string metaDataDestinationFileName = Path.Combine(destinationPath, metaDataFileName);

                                if (!File.Exists(metaDataDestinationFileName))
                                {
                                    strOracleSQLStatement =
                                        "SELECT a.PKGID, a.COL1, c.CREATORNAME, c.CREATEDATE, a.COL2, a.COL4, a.COL5, a.COL6, a.COL7, a.COL9, a.COL11, a.COL12, a.COL14, a.COL15, a.COL16, a.COL17, a.COL18, a.COL19, b.DESCRIPTION, b.UNIQUEID FROM WFUSER.WF_FIELDVALUE_7 a, PROCESS.WF_PKGATTACHMENT b, PROCESS.WF_PKGDEFINITION c WHERE a.PKGID = b.PKGID AND a.PKGID = c.PKGID AND b.PKGID = c.PKGID AND a.PKGID = " +
                                        oracleRow["PKGID"].ToString();

                                    DataSet dsMetadataRecords = new DataSet();

                                    dsMetadataRecords = DAL.GetOracleDataSet(strConnectionString, strOracleSQLStatement);

                                    using (FileStream fs = File.Create(metaDataFileName))

                                    using (StreamWriter writer = new StreamWriter(fs))
                                    {
                                        writer.WriteLine(
                                            "PackageId|ProjectName|PackageCreator|PackageCreateDate|CorpFinanceConsultant|SubjectMatter|Division|Year|DateOpened|RepLetter|Issue|Quarter|Topic|SubTopic|Section|Paragraph|AccountReference|SubjectMatter2|SubjectMatter3|FileName|FilePath");

                                        if (dsMetadataRecords.Tables.Count > 0)
                                        {
                                            foreach (DataRow metaDataRow in dsMetadataRecords.Tables[0].Rows)
                                            {
                                                string accountReference = metaDataRow["COL14"].ToString() + "-" + metaDataRow["COL15"].ToString() + "-" + metaDataRow["COL16"].ToString() + "-" + metaDataRow["COL17"].ToString();

                                                writer.WriteLine(metaDataRow["PKGID"].ToString() + "|" +
                                                                 metaDataRow["COL1"].ToString() + "|" +
                                                                 metaDataRow["CREATORNAME"].ToString() + "|" +
                                                                 metaDataRow["CREATEDATE"].ToString() + "|" +
                                                                 metaDataRow["COL2"].ToString() + "|" +
                                                                 metaDataRow["COL4"].ToString() + "|" +
                                                                 metaDataRow["COL5"].ToString() + "|" +
                                                                 metaDataRow["COL6"].ToString() + "|" +
                                                                 metaDataRow["COL7"].ToString() + "|" +
                                                                 metaDataRow["COL9"].ToString() + "|" +
                                                                 metaDataRow["COL11"].ToString() + "|" +
                                                                 metaDataRow["COL12"].ToString() + "|" +
                                                                 metaDataRow["COL14"].ToString() + "|" +
                                                                 metaDataRow["COL15"].ToString() + "|" +
                                                                 metaDataRow["COL16"].ToString() + "|" +
                                                                 metaDataRow["COL17"].ToString() + "|" +
                                                                 accountReference + "|" +
                                                                 metaDataRow["COL18"].ToString() + "|" +
                                                                 metaDataRow["COL19"].ToString() + "|" +
                                                                 metaDataRow["DESCRIPTION"].ToString() + "|" +
                                                                 metaDataRow["UNIQUEID"].ToString());
                                            }
                                        }
                                        else
                                        {
                                            writer.Write("No records found.");
                                        }

                                        writer.Close();
                                        fs.Close();
                                    }

                                    File.Copy(metaDataFileName, metaDataDestinationFileName, true);
                                }
                            }
                            catch (Exception ex)
                            {
                                logger.LogError("Metadata file write failed.");
                                logger.LogException(ex);
                            }
                        }

                        File.Copy(sourceFileName, destinationFileName, true);

                        logger.LogInfoMessage("File copied: " + destinationFileName);
                    }
                    catch (Exception fileException)
                    {
                        logger.LogInfoMessage("File not copied: " + destinationFileName + " - " + fileException.Message);
                        logger.LogException(fileException);
                    }

                    //logger.LogInfoMessage("Copied file: " + destinationFileName);

                    //// Use Path class to manipulate file and directory paths.
                    //string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                    //string destFile = System.IO.Path.Combine(targetPath, fileName);

                    //// To copy a folder's contents to a new location:
                    //// Create a new target folder, if necessary.
                    //if (!Directory.Exists(targetPath))
                    //{
                    //    Directory.CreateDirectory(targetPath);
                    //}

                    //// To copy a file to another location and 
                    //// overwrite the destination file if it already exists.
                    //System.IO.File.Copy(sourceFile, destFile, true);

                    //// To copy all the files in one directory to another directory.
                    //// Get the files in the source folder. (To recursively iterate through
                    //// all subfolders under the current directory, see
                    //// "How to: Iterate Through a Directory Tree.")
                    //// Note: Check for target path was performed previously
                    ////       in this code example.
                    //if (System.IO.Directory.Exists(sourcePath))
                    //{
                    //    string[] files = System.IO.Directory.GetFiles(sourcePath);

                    //    // Copy the files and overwrite destination files if they already exist.
                    //    foreach (string s in files)
                    //    {
                    //        // Use static Path methods to extract only the file name from the path.
                    //        fileName = System.IO.Path.GetFileName(s);
                    //        destFile = System.IO.Path.Combine(targetPath, fileName);
                    //        System.IO.File.Copy(s, destFile, true);
                    //    }
                    //}
                    //else
                    //{
                    //    Console.WriteLine("Source path does not exist!");
                    //}

                    //// Keep console window open in debug mode.
                    //Console.WriteLine("Press any key to exit.");
                    //Console.ReadKey();                                
                }

                //string strSWLServerSQLStatement;
                //strSWLServerSQLStatement = "DELETE FROM TPASControlData";
                //DAL.PerformSqlServerQuery(strConnectionString, strSWLServerSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message, ex);
            }
        }

        public static void InsertOracleRecords(string strConnectionString, DataSet dsOracleRecords)
        {
            try
            {
                DAL.PerformOracleInsert(strConnectionString, dsOracleRecords);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message, ex);
            }
        }

        public static void InsertSQLServerRecords(string strConnectionString, DataSet dsSQLServerRecords)
        {
            try
            {
                DAL.PerformSQLServerInsert(strConnectionString, dsSQLServerRecords);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message, ex);
            }
        }

        public static void InsertRemedyRecord(string strConnectionString, RemedyTicketInformation remedyTicketInfo)
        {
            string strSQLStatement = "";

            try
            {
                strSQLStatement =
                    "INSERT INTO [RemedyTicket] ([RemedyTicketDefinitionId], [RemedyTicketNumber], [Type], [SubType], [IsActive], " +
                    "[CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy]) " +
                    "VALUES (" + remedyTicketInfo.RemedyTicketDefinitionId + ", '" + remedyTicketInfo.RemedyTicketNumber +
                    "', '" + remedyTicketInfo.Type + "', '" + remedyTicketInfo.SubType +
                    "', '" + remedyTicketInfo.IsActive + "', '" + remedyTicketInfo.CreatedDate.ToShortDateString() + "', '" +
                    remedyTicketInfo.CreatedBy + "', '" + remedyTicketInfo.ModifiedDate.ToShortDateString() + "', '" +
                    remedyTicketInfo.ModifiedBy + "')";

                DAL.PerformRemedyRecordInsert(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message, ex);
            }
        }

        public static DataSet GetOracleRecords(string strConnectionString)
        {
            string strSQLStatement = "";

            DataSet dsOracleRecords = new DataSet();

            try
            {
                //strSQLStatement =
                //    "SELECT p.batch_id, p.document_id, e.create_dt FROM audit_event e, payment_req p WHERE E.PAYMENT_REQ_KEY = P.PAYMENT_REQ_KEY AND p.interface_origin_key = '14' AND E.AUDIT_INDEX = 0";

                strSQLStatement =
                    "SELECT b.DESCRIPTION, b.UNIQUEID FROM WFUSER.WF_FIELDVALUE_7 a, PROCESS.WF_PKGATTACHMENT b, PROCESS.WF_PKGDEFINITION c WHERE a.PKGID = b.PKGID AND a.PKGID = c.PKGID AND b.PKGID = c.PKGID";

                dsOracleRecords = DAL.GetOracleDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsOracleRecords;
        }

        public static DataSet GetSQLServerRecords(string strConnectionString)
        {
            string strSQLStatement = "";

            DataSet dsSQLServerRecords = new DataSet();

            try
            {
                //strSQLStatement =
                //    "SELECT   [batch].BatchID, [batch].CreationDTM, [document].DocNumber, BatchSrc = replace" +
                //    "((SELECT KeyValue AS [data()] FROM [DMSKCStats].[dbo].[BatchKeys] WHERE BatchID = [batch].BatchID ORDER BY BatchID FOR xml path('')), ' ', ' - ') " +
                //    "FROM             [DMSKCStats].[dbo].[Batch] [batch] " +
                //    "INNER JOIN       [DMSKCStats].[dbo].[Document] [document] ON [batch].BatchID = [document].BatchID AND [document].FormType = 'FT_Invoice' " +
                //    "LEFT OUTER JOIN  [DMSKCStats].[dbo].[BatchKeys] [batchkeys] ON batchkeys.BatchID = [batch].BatchID " +
                //    "GROUP BY         [batch].BatchID, [batch].CreationDTM, [document].DocNumber";

                strSQLStatement = "SELECT * FROM [TNSContractAdmin].[dbo].[TCD_IPM_Import]";

                dsSQLServerRecords = DAL.GetSQLServerDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsSQLServerRecords;
        }

        public static DataSet GetTcdIpmDistinctRecords(string strConnectionString)
        {
            string strSQLStatement = "";

            DataSet dsSQLServerRecords = new DataSet();

            try
            {
                //strSQLStatement = "SELECT TOP 1000 * FROM [TNSContractAdmin].[dbo].[TCD_IPM_Import]";
                //strSQLStatement =
                //    "SELECT * FROM [TNSContractAdmin].[dbo].[TCD_Distinct_IPM_RecIds]";   // WHERE [TNSContractAdmin].[dbo].[TCD_Distinct_IPM_RecIds].[IPM_RecId] > '147215528'";
                strSQLStatement =
                    "SELECT * FROM [TNSContractAdmin].[dbo].[TCD_Distinct_IPM_RecIds]";   // WHERE [TNSContractAdmin].[dbo].[TCD_Distinct_IPM_RecIds].[IPM_RecId] > '147215528'";

                dsSQLServerRecords = DAL.GetSQLServerDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsSQLServerRecords;
        }

        public static DataSet GetTcdIpmDistinctContentContractRecords(string strConnectionString)
        {
            string strSQLStatement = "";

            DataSet dsSQLServerRecords = new DataSet();

            try
            {
                strSQLStatement =
                    "SELECT DISTINCT([RECID]) " +
                    "FROM [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised] " +
                    "WHERE [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised].[MILLE_CONTRACT_ID] IS NOT NULL";
                    //" AND [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised].[DOCTYPE] != 'AMENDMENT'";

                dsSQLServerRecords = DAL.GetSQLServerDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsSQLServerRecords;
        }

        public static DataSet GetTcdIpmDistinctContractRecords(string strConnectionString)
        {
            string strSQLStatement = "";

            DataSet dsSQLServerRecords = new DataSet();

            try
            {
                strSQLStatement =
                    "SELECT DISTINCT([RECID]) " +
                    "FROM [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised] " +
                    "WHERE [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised].[MILLE_CONTRACT_ID] IS NOT NULL";                   // " " +
                    //"AND [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised].[DOCTYPE] != 'AMENDMENT'";

                dsSQLServerRecords = DAL.GetSQLServerDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsSQLServerRecords;
        }

        public static DataSet GetTcdIpmDistinctAffiliateContentRecords(string strConnectionString)
        {
            string strSQLStatement = "";

            DataSet dsSQLServerRecords = new DataSet();

            try
            {
                strSQLStatement =
                    "SELECT DISTINCT([RECID]) " +
                    "FROM [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised] " +
                    "WHERE [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised].[AFFILNBR] IS NOT NULL " +
                    "AND [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised].[MILLE_CONTRACT_ID] IS NULL";

                dsSQLServerRecords = DAL.GetSQLServerDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsSQLServerRecords;
        }

        public static DataSet GetTcdIpmDistinctAffiliateRecords(string strConnectionString)
        {
            string strSQLStatement = "";

            DataSet dsSQLServerRecords = new DataSet();

            try
            {
                strSQLStatement =
                    "SELECT DISTINCT([RECID]) " +
                    "FROM [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised] " +
                    "WHERE [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised].[AFFILNBR] IS NOT NULL " +
                    "AND [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised].[MILLE_CONTRACT_ID] IS NULL";

                dsSQLServerRecords = DAL.GetSQLServerDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsSQLServerRecords;
        }

        public static DataSet GetIntSalesRepositoryDataSet(string strConnectionString)
        {
            string strSQLStatement = "";

            DataSet dsOracleServerRecords = new DataSet();

            try
            {
                //strSQLStatement = "Select distinct EMEDIAMP.*,a.*,a.ORDERNBR|| \'_\' ||a.DOCTYPE|| \'_\' ||a.RECID As FileName  from CONTEXT.INTSALESMAIN a join CONTEXT.EMEDIAMP EMEDIAMP \r\non a.DOCUMENTTYPE= EMEDIAMP.PMID AND TO_CHAR(a.DATERECEIVED,\'YY\') = \'14\' ORDER BY a.RECID";
                //strSQLStatement = "Select distinct EMEDIAMP.*,a.*,a.ORDERNBR|| '_' ||a.DOCTYPE|| '_' ||a.RECID As FileName  from CONTEXT.INTSALESMAIN a join CONTEXT.EMEDIAMP EMEDIAMP on a.DOCUMENTTYPE= EMEDIAMP.PMID AND a.DATERECEIVED > to_date(\'31/12/14\', \'DD/MM/YY\') AND a.DATERECEIVED < to_date(\'01/07/15\', \'DD/MM/YY\') ORDER BY a.RECID";
                strSQLStatement = "Select distinct EMEDIAMP.*,a.*,a.ORDERNBR|| '_' ||a.DOCTYPE|| '_' ||a.RECID As FileName  from CONTEXT.INTSALESMAIN a join CONTEXT.EMEDIAMP EMEDIAMP on a.DOCUMENTTYPE= EMEDIAMP.PMID AND a.RECID IN (1489285142)";
                //strSQLStatement = "Select distinct a.COL12|| \'_\' ||a.COL5|| \'_PKG-\' ||c.PKGRECID as FileName, a.*, b.UNIQUEID, c.PKGRECID from WFUSER.WF_FIELDVALUE_11 a join  PROCESS.WF_PKGATTACHMENT b on a.PKGID = b.PKGID join PROCESS.WF_QUEUE c on b.PKGID = c.PKGID where is_number(b.UNIQUEID) = 0 AND TO_CHAR(a.COL15,\'YY\') = \'16\' ORDER By a.PKGID";

                logger.LogInfoMessage("IPM Query: " + strSQLStatement);

                dsOracleServerRecords = DAL.GetOracleDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsOracleServerRecords;
        }

        public static DataSet GetFarmRepositoryDataSet(string strConnectionString)
        {
            string strSQLStatement = "";

            DataSet dsOracleServerRecords = new DataSet();

            try
            {
                //strSQLStatement = "Select distinct EMEDIAMP.*,a.*,a.ORDERNBR|| \'_\' ||a.DOCTYPE|| \'_\' ||a.RECID As FileName  from CONTEXT.INTSALESMAIN a join CONTEXT.EMEDIAMP EMEDIAMP \r\non a.DOCUMENTTYPE= EMEDIAMP.PMID AND TO_CHAR(a.DATERECEIVED,\'YY\') = \'14\' ORDER BY a.RECID";
                //strSQLStatement = "Select distinct EMEDIAMP.*,a.*,a.ORDERNBR|| '_' ||a.DOCTYPE|| '_' ||a.RECID As FileName  from CONTEXT.INTSALESMAIN a join CONTEXT.EMEDIAMP EMEDIAMP on a.DOCUMENTTYPE= EMEDIAMP.PMID AND a.DATERECEIVED > to_date(\'31/12/14\', \'DD/MM/YY\') AND a.DATERECEIVED < to_date(\'01/07/15\', \'DD/MM/YY\') ORDER BY a.RECID";
                //strSQLStatement = "select distinct EMEDIAMP.*, a.* from CONTEXT.farmmain a join CONTEXT.EMEDIAMP EMEDIAMP on a.DOCUMENTTYPE= EMEDIAMP.PMID where to_char(recid) not in (Select distinct a.UNIQUEID from PROCESS.WF_PKGATTACHMENT a join  WFUSER.WF_FIELDVALUE_10 on a.PKGID = WFUSER.WF_FIELDVALUE_10.PKGID)";
                //strSQLStatement = "select distinct EMEDIAMP.*, a.* from CONTEXT.farmmain a join CONTEXT.EMEDIAMP EMEDIAMP on a.DOCUMENTTYPE= EMEDIAMP.PMID where to_char(recid) not in (Select distinct a.UNIQUEID from PROCESS.WF_PKGATTACHMENT a join  WFUSER.WF_FIELDVALUE_10 on a.PKGID = WFUSER.WF_FIELDVALUE_10.PKGID) and a.ORIGUPLOADDATE > to_date(\'31/12/12\', \'DD/MM/YY\') AND a.ORIGUPLOADDATE < to_date(\'01/01/14\', \'DD/MM/YY\') and a.RECID = \'1481151673\'";
                //strSQLStatement = "select distinct EMEDIAMP.*, a.* from CONTEXT.farmmain a join CONTEXT.EMEDIAMP EMEDIAMP on a.DOCUMENTTYPE= EMEDIAMP.PMID where to_char(recid) not in (Select distinct a.UNIQUEID from PROCESS.WF_PKGATTACHMENT a join  WFUSER.WF_FIELDVALUE_10 on a.PKGID = WFUSER.WF_FIELDVALUE_10.PKGID) and a.ORIGUPLOADDATE > to_date(\'31/12/12\', \'DD/MM/YY\') AND a.ORIGUPLOADDATE < to_date(\'01/01/14\', \'DD/MM/YY\')";
                //strSQLStatement = "select distinct EMEDIAMP.*, a.* from CONTEXT.farmmain a join CONTEXT.EMEDIAMP EMEDIAMP on a.DOCUMENTTYPE= EMEDIAMP.PMID where to_char(recid) not in (Select distinct a.UNIQUEID from PROCESS.WF_PKGATTACHMENT a join  WFUSER.WF_FIELDVALUE_10 on a.PKGID = WFUSER.WF_FIELDVALUE_10.PKGID)";
                strSQLStatement = "select distinct EMEDIAMP.*, a.* from CONTEXT.farmmain a join CONTEXT.EMEDIAMP EMEDIAMP on a.DOCUMENTTYPE= EMEDIAMP.PMID where to_char(recid) not in (Select distinct a.UNIQUEID from PROCESS.WF_PKGATTACHMENT a join  WFUSER.WF_FIELDVALUE_10 on a.PKGID = WFUSER.WF_FIELDVALUE_10.PKGID) and recid = \'1494872666\'";

                logger.LogInfoMessage("IPM Query: " + strSQLStatement);

                dsOracleServerRecords = DAL.GetOracleDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsOracleServerRecords;
        }

        public static DataSet GetFarmPackageDataSet(string strConnectionString)
        {
            string strSQLStatement = "";

            DataSet dsOracleServerRecords = new DataSet();

            try
            {
                //strSQLStatement = "select a.*,b.createdate from WF_FIELDVALUE_10 a join PROCESS.WF_PKGDEFINITION b on a.PKGID = b.PKGID where col12 = \'29-MAY-12\'";
                //strSQLStatement = "select a.*,b.createdate from WF_FIELDVALUE_10 a join PROCESS.WF_PKGDEFINITION b on a.PKGID = b.PKGID where a.col12 > to_date(\'31/03/12\', \'DD/MM/YY\') AND a.col12 < to_date(\'01/07/12\', \'DD/MM/YY\')";
                //strSQLStatement = "select a.*,b.createdate from WF_FIELDVALUE_10 a join PROCESS.WF_PKGDEFINITION b on a.PKGID = b.PKGID where a.col12 > to_date(\'30/06/15\', \'DD/MM/YY\') AND a.col12 < to_date(\'01/10/15\', \'DD/MM/YY\')";
                strSQLStatement = "select a.*,b.createdate from WF_FIELDVALUE_10 a join PROCESS.WF_PKGDEFINITION b on a.PKGID = b.PKGID where a.PKGID IN (\'2192516\')";
                //strSQLStatement = "select a.*,b.createdate from WF_FIELDVALUE_10 a join PROCESS.WF_PKGDEFINITION b on a.PKGID = b.PKGID where TO_CHAR(b.createdate,\'YY\') = \'14\'";
                
                logger.LogInfoMessage("GetFarmPackageDataSet IPM Query: " + strSQLStatement);

                dsOracleServerRecords = DAL.GetOracleDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsOracleServerRecords;
        }

        public static DataSet GetCartoonPackageDataSet(string strConnectionString)
        {
            string strSQLStatement = "";

            DataSet dsOracleServerRecords = new DataSet();

            try
            {
                //strSQLStatement = "select a.*,b.createdate from wf_fieldvalue_5 a join PROCESS.WF_PKGDEFINITION b on a.PKGID = b.PKGID order by b.createdate asc where a.PKGID IN (\'2192516\')";
                //strSQLStatement = "select a.*,b.createdate from wf_fieldvalue_5 a join PROCESS.WF_PKGDEFINITION b on a.PKGID = b.PKGID order by b.createdate asc";
                /*strSQLStatement = "select a.*,b.createdate from wf_fieldvalue_5 a join PROCESS.WF_PKGDEFINITION b on a.PKGID = b.PKGID where a.PKGID not in (\'1645979\'," +
                                  " \'1654772\',\'1672073\'" +
                                  ") order by b.createdate asc";*/
                strSQLStatement = "select a.*,b.createdate from wf_fieldvalue_5 a join PROCESS.WF_PKGDEFINITION b on a.PKGID = b.PKGID where a.PKGID = \'2075513\'";

                logger.LogInfoMessage("GetCartoonPackageDataSet IPM Query: " + strSQLStatement);

                dsOracleServerRecords = DAL.GetOracleDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsOracleServerRecords;
        }

        public static DataSet GetFarmPackageAttachs(string strConnectionString, string pkgId)
        {
            string strSQLStatement = "";

            DataSet dsOracleServerRecords = new DataSet();

            try
            {
                strSQLStatement = "Select a.* from PROCESS.WF_PKGATTACHMENT a join  WFUSER.WF_FIELDVALUE_10 on a.PKGID = WFUSER.WF_FIELDVALUE_10.PKGID where a.PKGID = \'" + pkgId + "\'";
                
                logger.LogInfoMessage("GetFarmPackageAttachs IPM Query: " + strSQLStatement);

                dsOracleServerRecords = DAL.GetOracleDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsOracleServerRecords;
        }

        public static DataSet GetFarmPackageHistory(string strConnectionString, string pkgId)
        {
            string strSQLStatement = "";

            DataSet dsOracleServerRecords = new DataSet();

            try
            {
                strSQLStatement = "SELECT AUDITORDER,AUDITSTAMP, TO_CHAR (AUDITSTAMP, \'MM/DD/YYYY HH:MI:SS AM\')as AUDITSTAMPTXT,pkgid,username,eventname,RESULTEVENTNAME,taskname,ResultProcessId,auditmsg, CASE  WHEN regexp_count(auditmsg,\'[^\"\"]+\') -1 > \'1\'  THEN \'1\'  WHEN regexp_count(auditmsg,\'[^\"\"]+\')-1 < \'1\'  THEN \'0\'  END AS EventResultReplace  FROM PROCESS.WF_AUDITLOG where pkgid= \'" + pkgId + "\' ORDER BY AUDITSTAMP ASC";

                //logger.LogInfoMessage("GetFarmPackageHistory IPM Query: " + strSQLStatement);

                dsOracleServerRecords = DAL.GetOracleDataSet(strConnectionString, strSQLStatement);
                //dsOracleServerRecords = DAL.GetOraclePagedDataSet(strConnectionString, strSQLStatement,1);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsOracleServerRecords;
        }

        public static DataSet GetFarmPackageHistoryCount(string strConnectionString, string pkgId)
        {
            string strSQLStatement = "";

            DataSet dsOracleServerRecords = new DataSet();

            try
            {
                strSQLStatement = "SELECT COUNT(*)  AS ENTRIES FROM PROCESS.WF_AUDITLOG where pkgid= \'" + pkgId + "\'";

                //logger.LogInfoMessage("GetFarmPackageHistory IPM Query: " + strSQLStatement);

                dsOracleServerRecords = DAL.GetOracleDataSet(strConnectionString, strSQLStatement);
                //dsOracleServerRecords = DAL.GetOraclePagedDataSet(strConnectionString, strSQLStatement,1);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsOracleServerRecords;
        }

        public static DataSet GetTcdIpmDistinctBusDevRecords(string strConnectionString)
        {
            string strSQLStatement = "";

            DataSet dsSQLServerRecords = new DataSet();

            try
            {
                strSQLStatement =
                    "SELECT DISTINCT([RECID]) FROM [TNSContractAdmin].[dbo].[TCD_Business_Development] ";

                dsSQLServerRecords = DAL.GetSQLServerDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsSQLServerRecords;
        }

        public static DataSet GetTcdIpmBusDevRecord(string strConnectionString, string recId)
        {
            string strSQLStatement = "";

            DataSet dsSQLServerRecords = new DataSet();

            try
            {
                strSQLStatement =
                    "SELECT * FROM [TNSContractAdmin].[dbo].[TCD_Business_Development] WHERE [RECID] = '" + recId + "'";

                dsSQLServerRecords = DAL.GetSQLServerDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsSQLServerRecords;
        }

        public static DataSet GetTcdIpmRecIdRecord(string strConnectionString, string recId)
        {
            string strSQLStatement = "";

            DataSet dsSQLServerRecords = new DataSet();

            try
            {
                strSQLStatement =
                    "SELECT * FROM [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised] WHERE [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised].[RECID] = '" +
                    recId + "' ORDER BY [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised].[FILEDDATE] DESC";

                dsSQLServerRecords = DAL.GetSQLServerDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsSQLServerRecords;
        }

        public static DataSet GetTcdIpmRecIdPageCountRecord(string strConnectionString)
        {
            string strSQLStatement = "";

            DataSet dsSQLServerRecords = new DataSet();

            try
            {
                //strSQLStatement =
                //    "SELECT DISTINCT([RECID]) " +
                //    "FROM [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised] " +
                //    "WHERE [AFFILNBR] IS NOT NULL " +
                //    "AND [MILLE_CONTRACT_ID] IS NULL " +
                //    "AND [RECID] IN (SELECT [RECID] FROM [TNSContractAdmin].[dbo].[TCD_RecId_PageCount]) ";

                strSQLStatement =
                    "SELECT DISTINCT([RECID]) " +
                    "FROM [TNSContractAdmin].[dbo].[TCD_IPM_Import_Revised] " +
                    "WHERE [MILLE_CONTRACT_ID] IS NOT NULL " +
                    "AND [RECID] IN (SELECT [RECID] FROM [TNSContractAdmin].[dbo].[TCD_RecId_PageCount]) ";

                dsSQLServerRecords = DAL.GetSQLServerDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsSQLServerRecords;
        }

        public static DataSet GetTcdIpmBDRecIdRecord(string strConnectionString, string recId)
        {
            string strSQLStatement = "";

            DataSet dsSQLServerRecords = new DataSet();

            try
            {
                strSQLStatement =
                    "SELECT * FROM [TNSContractAdmin].[dbo].[TCD_Businsess_Development] WHERE [TNSContractAdmin].[dbo].[TCD_Businsess_Development].[RECID] = '" +
                    recId + "' ORDER BY [TNSContractAdmin].[dbo].[TCD_Businsess_Development].[FILEDDATE] DESC";

                dsSQLServerRecords = DAL.GetSQLServerDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsSQLServerRecords;
        }

        public static void InsertTcdMigrationAuditRecords(string strConnectionString, string strSQLStatement)
        {
            try
            {
                DAL.ExecuteSQLStatement(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }
        }

        public static DataSet GetRemedyTicketDefinition(string strConnectionString)
        {
            string strSQLStatement = "";

            DataSet dsRemedyTicketDefinition = new DataSet();

            try
            {
                strSQLStatement =
                    "SELECT * FROM RemedyTicketDefinition";

                dsRemedyTicketDefinition = DAL.GetRemedyTicketDataSet(strConnectionString, strSQLStatement);
            }
            catch (Exception ex)
            {
                throw new Exception("BLL Exception Message : " + ex.Message + ", strSQLStatement : " + strSQLStatement, ex);
            }

            return dsRemedyTicketDefinition;
        }
    }
}
